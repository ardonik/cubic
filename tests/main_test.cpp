#include "gtest/gtest.h"
#include <memory>
#include <array>

// Demonstrate some basic assertions.
TEST(HelloTest, BasicAssertions) {
  // Expect two strings not to be equal.
  EXPECT_STRNE("hello", "world");
  // Expect equality.
  EXPECT_EQ(7 * 6, 42);
}

struct Color {
    public:
        double r, g, b, a; // All values range from 0 to 1.0
};


// A simple data structure that can represent faces, edges, and vertices in a
// cubic lattice.
struct Point {
    public:
        double x, y, z;
        Color color;
};

struct Edge {
    public:
        Edge(const Point& start, const Point& end) : start(start), end(end) { }
        const Point& start;
        const Point& end;
        Color color() const;
};

struct Face {
    public:
        const Point& ul;
        const Point& ur;
        const Point& lr;
        const Point& ll;
        Face(const Point& ul, const Point& ur, const Point& lr, const Point& ll) : ul(ul), ur(ur), lr(lr), ll(ll) { }
        const std::array<Point, 4> vertices() const { return std::array<Point, 4> { ul, ur, lr, ll }; }
        const std::array<Edge, 4> edges() const { return { Edge(ul, ur), Edge(ur, lr), Edge(lr, ll), Edge(ll, ul) }; }
        Color color() const;
};
