// =========================================================================
// Copyright (C) 2012 Ardonik <ardonik@ardonik.org>
//
// This file is part of ASC2OFF.
//
// ASC2OFF is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ASC2OFF is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ASC2OFF.  If not, see <http://www.gnu.org/licenses/>.
// =========================================================================

#include "generator.h"
#include "rgba.h"
#include "utilities.h"
#include <sstream>
#include <fstream>
#include <iostream>
#include <string>
#include <stdexcept>
#include <array>
#include <vector>
#include <map>
#include "UnitTest++.h"
#include <algorithm>

using std::stringstream;
using std::istringstream;
using std::ostringstream;
using std::runtime_error;
using std::ifstream;
using std::cout;
using std::array;
using std::find;
using std::map;
using std::string;
using std::vector;

/// A struct that can store persistent data for the tests; this allows the data
/// to only be initialized once, before any of the tests run.
struct TestFixture
{
    // Most Generators are local to the tests that they belong to (this keeps
    // exceptions from breaking the TestFixture constructor and causing *all*
    // of the tests to fail.)  There are a handful of Generators, however,
    // which will be useful to reuse.
    Generator long_arguments_generator;
    Generator short_arguments_generator;

    // A handful of random dimensions for the dimension deduction tests.
    int actual_width;
    int actual_height;
    int actual_depth;

    // String versions of the above.
    string w, h, d;

    // A string consisting of actual_width * actual_height * actual_depth characters.
    string data_string;

    /// Initialize some shared data structures and variables.
    TestFixture()
    {
        long_arguments_generator = Generator {
            "progname",
            "--width=3",
            "--height= 3",
            "--depth=3", // "--depth 3" not recognized by getopt for some reason
            // "--verbose",
            "--data=####-#####-#---#-#####-####",
            "-o /dev/null"
        };

        short_arguments_generator = Generator {
            "progname",
            "-w 3",
            "-h 3",
            "-d 3",
            // "-v",
            "-D@@@@-@@@@@-@---@-@@@@@-@@@@",
            "-o /dev/null"
        };

        // Set up the dimensions that the dimension deduction tests will
        // deduce.  I keep them small just to minimize the output during
        // verbose logging.
        actual_width = 1 + rand() % 3;
        actual_height = 1 + rand() % 3;
        actual_depth = 1 + rand() % 3;
        data_string = string(actual_width * actual_height * actual_depth, '@');

        // Convert the above dimensions to strings.
        stringstream stream;
        stream.str("");
        stream << actual_width;
        w = stream.str();
        stream.str("");
        stream << actual_height;
        h = stream.str();
        stream.str("");
        stream << actual_depth;
        d = stream.str();
    }
};


SUITE(Argument_Parsing)
{
    // Does ::trim() work correctly?
    TEST(TestTrimString) {
        CHECK_EQUAL("Hello", trim("\t Hello\n"));
        CHECK_EQUAL("Hello", trim("\b      \b Hello"));
        CHECK_EQUAL("Hello", trim("Hello\v\r\v"));
        CHECK_EQUAL("Hello", trim("Hello"));  // Identity case
        CHECK_EQUAL("", trim(""));            // Pathological input
        CHECK_EQUAL("", trim("\t"));          // More pathological input
        CHECK_EQUAL("", trim("\v\b\n\r\t ")); // More pathological input
    }

    // Does ::split() work correctly?
    TEST(TestSplitString) {
        vector<string> tokens;
        tokens = split("aa,bb,cc", ",");
        CHECK_EQUAL(3u, tokens.size());
        CHECK_EQUAL("aa", tokens.at(0));
        CHECK_EQUAL("bb", tokens.at(1));
        CHECK_EQUAL("cc", tokens.at(2));

        tokens = split("/aa/bb", "/"); // Returning an initial empty token helps to distinguish "/aa/bb" from "aa/bb".
        CHECK_EQUAL(3u, tokens.size());
        CHECK_EQUAL("",   tokens.at(0));
        CHECK_EQUAL("aa", tokens.at(1));
        CHECK_EQUAL("bb", tokens.at(2));

        tokens = split(";:aa:;", ";:");
        CHECK_EQUAL(5u, tokens.size());
        CHECK_EQUAL("",   tokens.at(0));
        CHECK_EQUAL("",   tokens.at(1));
        CHECK_EQUAL("aa", tokens.at(2));
        CHECK_EQUAL("",   tokens.at(3));
        CHECK_EQUAL("",   tokens.at(4));

        tokens = split("test", "$"); // No delimiters = one big token.
        CHECK_EQUAL(1u, tokens.size());
        CHECK_EQUAL("test", tokens.at(0));
    }

    // Did our command line parser read the dimension arguments correctly?
    TEST_FIXTURE(TestFixture, TestParseDimensions) {
        CHECK_EQUAL(3, long_arguments_generator.width());
        CHECK_EQUAL(3, long_arguments_generator.height());
        CHECK_EQUAL(3, long_arguments_generator.depth());
        CHECK_EQUAL(3, short_arguments_generator.width());
        CHECK_EQUAL(3, short_arguments_generator.height());
        CHECK_EQUAL(3, short_arguments_generator.depth());
    }

    // I don't think this test is relevant anymore, now that we actually
    // validate dimensions.  Surely there are other tests which cover things
    // like the -o flag.
    //
    // Maybe we should assert that the constructor throws when given no
    // arguments, since it obviously would not have any valid dimensions to
    // work with!  I dunno.
    TEST(TestDefaultArguments) {
        // Provide the absolute minimum argument set.
        Generator generator = Generator {
            "progname",
            "-o /dev/null"
        };
        CHECK_EQUAL(0, generator.width());
        CHECK_EQUAL(0, generator.height());
        CHECK_EQUAL(0, generator.depth());
        CHECK_EQUAL(false, generator.verbose());
        CHECK_EQUAL("", generator.input());

        // ...except for this one.  If we didn't supply this argument, the
        // test would spam an empty OFF file to stdout.
        CHECK_EQUAL("/dev/null", generator.output());
    }

    // Can we still generate a structure correctly even when we don't pass in
    // any named arguments?
    TEST(TestUnnamedArguments) {
        Generator unnamed_arguments_generator = Generator {
            "progname",
            "-o /dev/null",
            "3",
            "2",
            "1",
            "%-%-%-",
        };

        CHECK_EQUAL(3, unnamed_arguments_generator.width());
        CHECK_EQUAL(2, unnamed_arguments_generator.height());
        CHECK_EQUAL(1, unnamed_arguments_generator.depth());
        CHECK_EQUAL("%-%-%-", unnamed_arguments_generator.data());
    }

    // Does passing in certain arguments without an associated value cause an
    // exception to be thrown?
    TEST(TestMissingRequiredArguments) {
        CHECK_THROW(Generator({ "progname", "-w"        }), runtime_error);
        CHECK_THROW(Generator({ "progname", "--width"   }), runtime_error);
        CHECK_THROW(Generator({ "progname", "-h"        }), runtime_error);
        CHECK_THROW(Generator({ "progname", "--height"  }), runtime_error);
        CHECK_THROW(Generator({ "progname", "-d"        }), runtime_error);
        CHECK_THROW(Generator({ "progname", "--depth"   }), runtime_error);
        CHECK_THROW(Generator({ "progname", "-D"        }), runtime_error);
        CHECK_THROW(Generator({ "progname", "--data"    }), runtime_error);
        CHECK_THROW(Generator({ "progname", "-c"        }), runtime_error);
        CHECK_THROW(Generator({ "progname", "--colors"  }), runtime_error);
        CHECK_THROW(Generator({ "progname", "-i"        }), runtime_error);
        CHECK_THROW(Generator({ "progname", "--input"   }), runtime_error);
        CHECK_THROW(Generator({ "progname", "-o"        }), runtime_error);
        CHECK_THROW(Generator({ "progname", "--output"  }), runtime_error);
    }

    // Does passing in unrecognized short or long arguments cause an exception
    // to be thrown?
    TEST(TestUnrecognizedArguments) {
        CHECK_THROW(Generator({ "progname",                   "-B" }), runtime_error);
        CHECK_THROW(Generator({ "progname", "--cow=Holstein", "-B" }), runtime_error);
    }

    // Does reading from a missing input file cause an exception to be thrown?
    TEST(TestReadFromMissingFile) {
        CHECK_THROW(
            Generator ({
                "progname",
                "-v",
                "--input=/path/to/file/which/does/not/exist",
            }),
            runtime_error);
    }

    // Do data strings from standard input get merged with data strings from
    // the argument list?
    TEST(TestCombineStandardInputWithDataArgument) {
        // Arbitrarily divide the data string between "standard input" and the
        // data argument.
        const string original_input_string = "$..$$..$$..$.$$..$$..$$.";
        int offset = std::rand() % original_input_string.size();

        string left = original_input_string.substr(0, offset);
        string right = original_input_string.substr(offset);
        stringstream input_stream(left);

        Generator mixed_arguments_generator (
            {
                "progname",
                "-w 4",
                "3",            // Will become the depth regardless of position!
                "--height=2",
                right,          // Should be appended to the input stream's contents.
                "-i -",         // Tell the generator that there IS an input stream!
                //"-v",         // For debugging.
                "-o /dev/null",
            },
            input_stream,
            cout);
        CHECK_EQUAL(original_input_string, mixed_arguments_generator.data());
    }

    // If a data string is shorter than the known dimensions, is it padded
    // with whitespace so its length equals their product?
    TEST(TestPadShortDataString) {
        const int difference = 5;
        string data_string = string(3 * 3 * 3 - difference, '@');
        Generator generator ({
                "progname",
                "--width=3",
                "--height=3",
                "--depth=3",
                "--data=" + data_string,
                "-o /dev/null",
        });

        // Did we pad?
        CHECK_EQUAL(data_string + string(difference, ' '), generator.data());
    }

    // If a data string is longer than the known dimensions, is it truncated
    // so its length equals their product?
    TEST(TestTruncateLongDataString) {
        const int difference = 10;
        string data_string = string(3 * 3 * 3 + difference, '@');
        stringstream input_stream(data_string);
        Generator generator (
            {
                "progname",
                "-i -",  // Without this argument, we'd get 27 characters of padding!
                "--width=3",
                "--height=3",
                "--depth=3",
                // "-v", // For debugging
                "-o /dev/null",
            },
            input_stream, // We should truncate regardless of where the data came from.
            cout);

        // Did we truncate?
        CHECK_EQUAL(data_string.size() - difference, generator.data().size());
        CHECK_EQUAL(data_string.substr(0, data_string.size() - difference), generator.data());
    }

    // Can we define an input string using newline-separated,
    // whitespace-padded lines and have it be interpreted correctly?
    TEST(TestTrimPaddingFromDataStringInputLines) {
        const string data_string =
            "\t  .1.  \b\v\n"
            "\t  1.1  \b\v\n"
            "\t  .1.  \b\v\n"
            "\t       \b\v\n"
            "\t  2.2  \b\v\n"
            "\t  .2.  \b\v\n"
            "\t  2.2  \b\v\n"
            "\t       \b\v\n"
            "\t  .3.  \b\v\n"
            "\t  3.3  \b\v\n"
            "\t  .3.  \b\v\n";
        Generator generator = Generator {
            "progname",
            "--width=3",
            "--height=3",
            "--depth=3",
            "-o /dev/null",
            //"-v",
            "--data=" + data_string,
        };

        CHECK_EQUAL(13u, generator.cube_table().size());
    }

    // Can we parse SOMA input files?
    TEST(TestReadSomaInputFile) {
        const array<RGBA, 7> standard_soma_colors = {
            {
                RGBA("#ff00ff"), // SOMA puzzle piece #1, "The V"
                RGBA("#ff0000"), // SOMA puzzle piece #2, "The L"
                RGBA("#ffff00"), // SOMA puzzle piece #3, "The T"
                RGBA("#0000ff"), // SOMA puzzle piece #4, "The Z"
                RGBA("#00ff00"), // SOMA puzzle piece #5, "The A"
                RGBA("#c0c0c0"), // SOMA puzzle piece #6, "The B"
                RGBA("#00ffff"), // SOMA puzzle piece #7, "The P"
            }
        };

        // Load an *actual* SOMA puzzle solution file.
        ifstream soma_file("./samples/SOMA001.txt");
        bool soma_file_exists = (static_cast<bool>(soma_file) == true);
        CHECK(soma_file_exists);

        Generator generator (
            {
                "progname",
                "-o /dev/null",
                "-i -",
                // "-v",
            },
            soma_file, // Width, height, and depth should be deduced automatically.
            cout);

        // The result should have 27 cubes total.
        CHECK_EQUAL(27u, generator.cube_table().size());

        // Check to see that the right number of cubes have each assigned
        // color.
        map<RGBA, int> unique_color_counts;
        for (auto iter = generator.cube_table().begin(); iter != generator.cube_table().end(); ++iter) {
            unique_color_counts[iter->color()]++;
        }
        CHECK_EQUAL(3, unique_color_counts[standard_soma_colors[0]]); // SOMA puzzle piece #1, "The V"
        CHECK_EQUAL(4, unique_color_counts[standard_soma_colors[1]]); // SOMA puzzle piece #2, "The L"
        CHECK_EQUAL(4, unique_color_counts[standard_soma_colors[2]]); // SOMA puzzle piece #3, "The T"
        CHECK_EQUAL(4, unique_color_counts[standard_soma_colors[3]]); // SOMA puzzle piece #4, "The Z"
        CHECK_EQUAL(4, unique_color_counts[standard_soma_colors[4]]); // SOMA puzzle piece #5, "The A"
        CHECK_EQUAL(4, unique_color_counts[standard_soma_colors[5]]); // SOMA puzzle piece #6, "The B"
        CHECK_EQUAL(4, unique_color_counts[standard_soma_colors[6]]); // SOMA puzzle piece #7, "The P"
    }

    // Given the height and the depth, can we figure out the width
    // automatically?
    TEST_FIXTURE(TestFixture, TestDeduceWidth)
    {
        Generator missing_width_generator = Generator {
            "progname",
            "--output=/dev/null",
            "--data=" + data_string,
            "--height=" + h,
            "--depth=" + d,
            // "-v",
        };

        // Did we figure out the width?
        CHECK_EQUAL(actual_width, missing_width_generator.width());
    }

    // Given the width and the depth, can we figure out the height
    // automatically?
    TEST_FIXTURE(TestFixture, TestDeduceHeight)
    {
        Generator missing_height_generator = Generator {
            "progname",
            "--output=/dev/null",
            "--data=" + data_string,
            "--width=" + w,
            "--depth=" + d,
            // "-v",
        };

        // Did we figure out the height?
        CHECK_EQUAL(actual_height, missing_height_generator.height());
    }

    // Given the width and the height, can we figure out the depth
    // automatically?
    TEST_FIXTURE(TestFixture, TestDeduceDepth) {

        Generator missing_depth_generator = Generator {
            "progname",
            "--output=/dev/null",
            "--data=" + data_string,
            "--width=" + w,
            "--height=" + h,
            // "-v",
        };

        // Did we figure out the depth?
        CHECK_EQUAL(actual_depth, missing_depth_generator.depth());
    }

    // Given two dimensions and an intentionally short data string, will the
    // missing dimension be deduced as 1 with appropriate padding applied
    // afterward?
    TEST_FIXTURE(TestFixture, TestDeduceDimensionsWithPadding)
    {
        // Create a data string which is deliberately shorter than the product
        // of two existing dimensions.
        //
        // This should cause the deduced value for the missing dimension to
        // become 1 (the minimum value), and the Generator should add blank
        // padding to the string *anyway*.

        string short_data_string(actual_width * actual_height - 1, '$');

        Generator missing_depth_generator = Generator {
            "progname",
            "--output=/dev/null",
            "--data=" + short_data_string,
            "--width=" + w,
            "--height=" + h,
            // "-v",
        };

        // Is the deduced missing dimension minimal?
        CHECK_EQUAL(1, missing_depth_generator.depth());

        // Padding or not, we should have one cube for every character in that
        // data string.
        CHECK_EQUAL(short_data_string.length(), missing_depth_generator.cube_table().size());

        // When combined with the padding, the final data string should have a
        // well-defined length equal to the product of all the dimensions.
        CHECK_EQUAL(actual_width * actual_height * 1u, missing_depth_generator.data().length());

        // The first part of the data string should obviously be equal to what
        // we passed in.
        CHECK_EQUAL(short_data_string, missing_depth_generator.data().substr(0, short_data_string.length()));

        // The remaining part of the string (the padding) should have nothing
        // but blank characters in it.
        string remainder = missing_depth_generator.data().substr(short_data_string.length());
        CHECK(remainder.find_first_not_of(" .-") == string::npos);
    }

    // Given two dimensions and an intentionally long data string, will the
    // missing dimension be deduced as 1 with appropriate truncation applied
    // afterward?
    TEST(TestDeduceDimensionsWithTruncation)
    {

    }

    // Given a data string and an argument of "--geometry N", can we
    // successfully treat the data as a linear sequence of that many cubes?
    TEST_FIXTURE(TestFixture, TestParseGeometryWx1x1)
    {
        string data_string = string(actual_width * actual_height * actual_depth, '@');
        Generator linear_generator = Generator {
            "progname",
            "--output=/dev/null",
            "--data=" + data_string,
            "--geometry=" + w,
            "-v",
        };
        // The deduced height and depth should both be 1, and the width should
        // be as specified.
        CHECK_EQUAL(1, linear_generator.height());
        CHECK_EQUAL(1, linear_generator.depth());
        CHECK_EQUAL(actual_width, linear_generator.width());

        // There is going to be extra padding--precisely [(w * h * d) - w]
        // characters of it.  The generated data string should differ from
        // ours by exactly that much.
        CHECK_EQUAL(actual_width * (actual_height * actual_depth - 1),
                    int(data_string.length() - linear_generator.data().length()));

        // The first part of the data string should obviously be equal to what
        // we passed in.
        CHECK_EQUAL(data_string.substr(0, linear_generator.data().length()), linear_generator.data());

        // There should be exactly width cubes in the generated string.
        CHECK_EQUAL(actual_width, int(linear_generator.cube_table().size()));
    }

    TEST_FIXTURE(TestFixture, TestParseGeometryWxHx1)
    {
        string data_string = string(actual_width * actual_height * actual_depth, '@');
        Generator planar_generator = Generator {
            "progname",
            "--output=/dev/null",
            "--data=" + data_string,
            "--geometry=" + w + "x" + h,
            "-v",
        };

        // The width and height should be as specified.  Based on the length
        // of the data string, the deduced depth should be exactly equal to
        // the actual depth.  (In short, all three parameters should be set correctly.)
        CHECK_EQUAL(1, planar_generator.height());
        CHECK_EQUAL(1, planar_generator.depth());
        CHECK_EQUAL(actual_width, planar_generator.width());

        // Our data string should be exactly the same as the generated one.
        CHECK_EQUAL(data_string, planar_generator.data());
    }

    TEST_FIXTURE(TestFixture, TestParseGeometryWxHxD)
    {
        string data_string = string(actual_width * actual_height * actual_depth, '@');
        Generator planar_generator = Generator {
            "progname",
            "--output=/dev/null",
            "--data=" + data_string,
            "--geometry=" + w + "x" + h + "x" + d,
            "-v",
        };
    }

} // end (SUITE(Argument_Parsing))

SUITE(Intermediate_Structure_Generation)
{
    // In a lattice of a given size, there is a one-to-one (bijective) mapping
    // between coordinates and their offset equivalents.
    TEST(TestKnownOffsets) {
        CHECK_EQUAL(0,   Generator::offset(Point(0, 0, 0), 10, 10, 10));
        CHECK_EQUAL(1,   Generator::offset(Point(1, 0, 0), 10, 10, 10));
        CHECK_EQUAL(10,  Generator::offset(Point(0, 1, 0), 10, 10, 10));
        CHECK_EQUAL(100, Generator::offset(Point(0, 0, 1), 10, 10, 10));
        CHECK_EQUAL(999, Generator::offset(Point(9, 9, 9), 10, 10, 10));
    }

    // The reverse of TestKnownOffsets.
    TEST(TestKnownCoordinates) {
        CHECK(Point(0, 0, 0) == Generator::coordinate(0,   10, 10, 10));
        CHECK(Point(1, 0, 0) == Generator::coordinate(1,   10, 10, 10));
        CHECK(Point(0, 1, 0) == Generator::coordinate(10,  10, 10, 10));
        CHECK(Point(0, 0, 1) == Generator::coordinate(100, 10, 10, 10));
        CHECK(Point(9, 9, 9) == Generator::coordinate(999, 10, 10, 10));
    }

    // This test ensures that the offset formula is actually giving me the
    // mathematically correct result.
    TEST(TestOffsetCoordinateBijection) {
        int width = 10, height = 10, depth = 10;
        Point p(5, 6, 7);
        int expected_offset = 10 * 10 * 7 + 10 * 6 + 5;
        int actual_offset = Generator::offset(p, width, height, depth);
        CHECK_EQUAL(expected_offset, actual_offset);
        Point actual_coordinate = Generator::coordinate(actual_offset, width, height, depth);
        CHECK_EQUAL(p.x, actual_coordinate.x);
        CHECK_EQUAL(p.y, actual_coordinate.y);
        CHECK_EQUAL(p.z, actual_coordinate.z);
    }

    // Can we copy Vertex objects correctly?
    // Not so relevant now that the Vertex class's copy constructor has been
    // removed, but I'll keep it anyway.
    TEST(TestVertexCopyConstructor) {
        Vertex v;
        v.x = 1;
        v.y = 2;
        v.z = 3;
        Vertex copy(v);
        CHECK_EQUAL(v.x, copy.x);
        CHECK_EQUAL(v.y, copy.y);
        CHECK_EQUAL(v.z, copy.z);
    }

    // The lattice of vertices created for a WxHxD generator should all have
    // the correct offsets.
    TEST_FIXTURE(TestFixture, TestVertexLatticeGeneration) {

        Generator& generator = long_arguments_generator;

        size_t expected_size = (generator.width() + 1) *
                               (generator.height() + 1) *
                               (generator.depth() + 1);

        CHECK(generator.width() > 0);
        CHECK(generator.height() > 0);
        CHECK(generator.depth() > 0);
        CHECK_EQUAL(generator.vertex_lattice().size(),
                    expected_size);

        // Were the coordinates recorded correctly and in the right order?
        for (int w = 0; w < generator.width() + 1; ++w) {
            for (int h = 0; h < generator.height() + 1; ++h) {
                for (int d = 0; d < generator.depth() + 1; ++d) {
                    int current_offset = generator.offset(Point(w, h, d), generator.width() + 1, generator.height() + 1, generator.depth() + 1);
                    Vertex current_vertex = generator.vertex_lattice()[current_offset];
                    CHECK_EQUAL(w, current_vertex.x);
                    CHECK_EQUAL(h, current_vertex.y);
                    CHECK_EQUAL(d, current_vertex.z);
                }
            }
        }
    }

    // Do we create the correct number of internal faces, with no overlap or
    // duplication?
    TEST(TestDuplicateFaceRemoval) {
        Generator generator = Generator {
            "progname",
            "-o /dev/null",
            "2",
            "1",
            "1",
            "@@",
            // "-v" // For debugging, mostly.
        };

        // A 2x1x1 cubic lattice should have only 11 faces, not 12.
        CHECK_EQUAL(11u, generator.face_table().size());
    }

    // When requested by the user, do we correctly remove any faces, edges,
    // and vertices that ought to be hidden in an opaque object?
    TEST(TestHiddenFaceAndEdgeRemoval) {
        string data =
            "/SOMA\n"
            "; 2x2x2 Test cube.\n"
            " /12/21\n"
            " /34/43\n";
        stringstream data_stream(data);
        Generator generator = Generator (
            {
                "progname",
                "-o /dev/null",
                "--width=666",  // Ignored for SOMA input
                "--depth=777",  // Ignored for SOMA input
                "--height=999", // Ignored for SOMA input
                "-i -",
                "--optimize",
                    // "-v"
            },
            data_stream,
            cout);

        vector<Vertex> vertex_table;
        vector<Edge> edge_table;
        vector<Face> face_table;
        vector<Cube> cube_table;
        generator.create_compact_tables(vertex_table, edge_table, face_table, cube_table);

        // The Generator should still have eight cubes after hidden face
        // removal...
        CHECK_EQUAL(8u, cube_table.size());

        // ...But only 4 * 6 = 24 faces....
        CHECK_EQUAL(24u, face_table.size());

        // ...(6 * 4) * 4/2 = 48 edges (all on the surface)....
        CHECK_EQUAL(48u, edge_table.size());

        // ...and (3 * 3 * 3) - 1 = 26 vertices.
        CHECK_EQUAL(26u, vertex_table.size());
    }


    // This is the most important test of all!
    TEST(TestCubicLatticeGeneration) {

        // Use a special 2-cube generator with known characteristics.
        Generator generator = Generator {
            "progname",
            "-w 2",
            "-h 2",
            "-d 2",
            "-D *------*",
            "-o test.off"
        };

        CHECK_EQUAL(2u, generator.cube_table().size());

        ///////////////////////////////////////////////////////
        // Are the generated cubes in the correct positions? //

        Cube cube1 = generator.cube_table()[0];
        Cube cube2 = generator.cube_table()[1];
        CHECK(cube1.coordinate() == Point(0, 0, 0));
        CHECK(cube2.coordinate() == Point(1, 1, 1));

        ////////////////////////////////////////////////////////////
        // Does each cube have the correct set of eight vertices? //

        vector<int> expected_vertex_offsets[2] = {
            { 0,  1,  3,  4,  9,  10, 12, 13 }, // Cube #0, position: 0, 0, 0.
            { 13, 14, 16, 17, 22, 23, 25, 26 }  // Cube #1, position: 1, 1, 1.
        };
        for (int cube_index = 0; cube_index < 2; ++cube_index) {
            const Cube& current_cube = generator.cube_table()[cube_index];

            for (auto iter = expected_vertex_offsets[cube_index].begin(); iter != expected_vertex_offsets[cube_index].end(); ++iter) {
                int vertex_offset = *iter;

                // Verify that the cube "owns" this vertex.
                CHECK(find(current_cube.vertices().begin(), current_cube.vertices().end(), vertex_offset) != current_cube.vertices().end());

                // Verify that this vertex counts this cube's index as one of
                // its "parents."
                const Vertex& current_vertex = generator.vertex_lattice()[vertex_offset];
                CHECK(find(current_vertex.cubes().begin(), current_vertex.cubes().end(), cube_index) != current_vertex.cubes().end());
            }
        }

        ///////////////////////////////////////////////////////
        // Does each cube have the correct set of six faces? //

        vector<vector<vector<int> > > expected_face_vertex_indices;

        expected_face_vertex_indices.insert(expected_face_vertex_indices.end(), vector<vector<int> > {
                { 0, 1, 10, 9 },   // Cube #0, front
                { 3, 4, 13, 12 },  // Cube #0, back
                { 0, 3, 12, 9 },   // Cube #0, left
                { 1, 4, 13, 10 },  // Cube #0, right
                { 0, 1, 4, 3 },    // Cube #0, top
                { 9, 10, 13, 12 }, // Cube #0, bottom
        });

        expected_face_vertex_indices.insert(expected_face_vertex_indices.end(), vector<vector<int> > {
                { 13, 14, 23, 22 }, // Cube #1, front
                { 16, 17, 26, 25 }, // Cube #1, back
                { 13, 16, 25, 22 }, // Cube #1, left
                { 14, 17, 26, 23 }, // Cube #1, right
                { 13, 14, 17, 16 }, // Cube #1, top
                { 22, 23, 26, 25 }, // Cube #1, bottom
        });

        CHECK_EQUAL(12u, generator.face_table().size());
        if (generator.face_table().size() != 12) {
            cout << "ERROR: 2 disjoint cubes should yield 12 faces.  Things are just too weird for me right now; please fix your face generation code.\n";
            return;
        }

        for (size_t cube_index = 0; cube_index < expected_face_vertex_indices.size(); ++cube_index) {
            const Cube& cube = generator.cube_table()[cube_index];

            for (size_t face_index = 0; face_index < 6; ++face_index) {
                // The cube must have a face with these four vertices.  Search for
                // it.
                int matching_face = -1;
                for (size_t i = 0; i < cube.faces().size(); ++i) {
                    const Face& face = generator.face_table()[cube.faces()[i]];

                    // Does this face count the current cube as a parent?
                    CHECK(find(face.cubes().begin(), face.cubes().end(), cube_index) != face.cubes().end());

                    // Do all four expected vertices match this face?
                    Face temp;
                    temp.vertices()[0] = expected_face_vertex_indices[cube_index][face_index][0];
                    temp.vertices()[1] = expected_face_vertex_indices[cube_index][face_index][1];
                    temp.vertices()[2] = expected_face_vertex_indices[cube_index][face_index][2];
                    temp.vertices()[3] = expected_face_vertex_indices[cube_index][face_index][3];
                    if (face.has_same_vertices(temp)) {
                        matching_face = i;
                    }

                } // end (for each real face in the current cube)

                // Comments that will be useful if this test ever fails.
                // cout << "TestCubicLatticeGeneration: Cube #" << cube_index << ": ";
                // if (matching_face >= 0) {
                //     cout << "Face #" << matching_face;
                // } else {
                //     cout << "NO face";
                // }
                // cout << " matches these vertex indices: { "
                //      << expected_face_vertex_indices[cube_index][face_index][0] << ", "
                //      << expected_face_vertex_indices[cube_index][face_index][1] << ", "
                //      << expected_face_vertex_indices[cube_index][face_index][2] << ", "
                //      << expected_face_vertex_indices[cube_index][face_index][3] << " }\n";

                // Did ANY of the faces of the current cube match the expected vertices?
                CHECK(matching_face >= 0);

            } // end (for each hard-coded expected face in the current cube)
        } // end (for each cube)

        //////////////////////////////////////////////////////
        // Does each cube have the correct set of 12 edges? //

        vector<vector<vector<int> > > expected_edge_vertex_indices;

        expected_edge_vertex_indices.insert(expected_edge_vertex_indices.end(), vector<vector<int> > {
                { 0, 1 },   // Cube #0, front top
                { 1, 10 },  // Cube #0, front right
                { 10, 9 },  // Cube #0, front bottom
                { 9, 0 },   // Cube #0, front left
                { 0, 3 },   // Cube #0, left top
                { 1, 4 },   // Cube #0, right top
                { 10, 13 }, // Cube #0, right bottom
                { 9, 12 },  // Cube #0, left bottom
                { 3, 4 },   // Cube #0, rear top
                { 4, 13 },  // Cube #0, rear right
                { 13, 12 }, // Cube #0, rear bottom
                { 12, 3 },  // Cube #0, rear left
        });

        expected_edge_vertex_indices.insert(expected_edge_vertex_indices.end(), vector<vector<int> > {
                { 13, 14 }, // Cube #1, front top
                { 14, 23 }, // Cube #1, front right
                { 23, 22 }, // Cube #1, front bottom
                { 22, 13 }, // Cube #1, front left
                { 13, 16 }, // Cube #1, left top
                { 14, 17 }, // Cube #1, right top
                { 23, 26 }, // Cube #1, right bottom
                { 22, 25 }, // Cube #1, left bottom
                { 16, 17 }, // Cube #1, rear top
                { 17, 26 }, // Cube #1, rear right
                { 26, 25 }, // Cube #1, rear bottom
                { 25, 16 }, // Cube #1, rear left
        });

        CHECK_EQUAL(24u, generator.edge_table().size());
        if (generator.edge_table().size() != 24) {
            cout << "ERROR: 2 disjoint cubes should yield 24 edges.  Things are just too weird for me right now; please fix your edge generation code.\n";
            return;
        }

        for (size_t cube_index = 0; cube_index < expected_edge_vertex_indices.size(); ++cube_index) {
            const Cube& cube = generator.cube_table()[cube_index];

            // Check to see that the cube has all twelve of the expected
            // edges.
            for (size_t actual_edge_index = 0; actual_edge_index < 12; ++actual_edge_index) {
                const Edge& actual_edge = generator.edge_table()[cube.edges()[actual_edge_index]];

                // Does this edge count the current cube as a parent?
                CHECK(find(actual_edge.cubes().begin(), actual_edge.cubes().end(), cube_index) != actual_edge.cubes().end());

                // Does each edge have the two expected vertices?
                int matching_expected_edge_index = -1;
                for (int expected_edge_index = 0; expected_edge_index < 12; ++expected_edge_index) {
                    Edge expected_edge;
                    expected_edge.vertices()[0] = expected_edge_vertex_indices[cube_index][expected_edge_index][0];
                    expected_edge.vertices()[1] = expected_edge_vertex_indices[cube_index][expected_edge_index][1];
                    if (actual_edge.has_same_vertices(expected_edge)) {
                        matching_expected_edge_index = expected_edge_index;
                    }
                } // end (for each of the 12 expected edges)

                // More comments that will be useful if this test ever fails.
                // cout << "TestCubicLatticeGeneration: Cube #" << cube_index
                //      << ": the vertices of edge #" << actual_edge_index << " ("
                //      << generator.edge_table()[cube.edges()[actual_edge_index]] << ") ";
                // if (matching_expected_edge_index >= 0) {
                //     cout << "matched";
                // } else {
                //     cout << "did NOT match";
                // }
                // cout << " an expected edge index.\n";

                // Did ANY of the edges of the current cube match the expected vertices?
                CHECK(matching_expected_edge_index >= 0);

            } // end (for each actual edge in the current cube)
        } // end (for each cube)
    }

} // end (SUITE(Intermediate_Structure_Generation))

SUITE(Colors) {
    TEST(TestReadBadColors) {
        RGBA not_a_color, no_initial_hex_digit, invalid0, invalid1, invalid2;
        RGBA invalid5, invalid7, too_large;
        string token;
        stringstream stream("nonsense #garbage # #0 #00 #00000 #0000000 #0123456789abcdef");

        stream >> not_a_color;
        CHECK(!stream);
        stream.clear();
        stream >> token; // Eliminate "nonsense" from the input stream.
        CHECK_EQUAL("nonsense", token);

        stream >> no_initial_hex_digit;
        CHECK(!stream);
        stream.clear();
        stream >> token; // Eliminate "garbage" from the input stream (we already ate the "#" in front of it!)
        CHECK_EQUAL("garbage", token);

        stream >> invalid0;
        CHECK(!stream);
        stream.clear();

        stream >> invalid1;
        CHECK(!stream);
        stream.clear();

        stream >> invalid2;
        CHECK(!stream);
        stream.clear();

        stream >> invalid5;
        CHECK(!stream);
        stream.clear();

        stream >> invalid7;
        CHECK(!stream);
        stream.clear();

        stream >> too_large;
        CHECK(!stream);
        stream.clear();
    }

    TEST(TestReadValidColors) {
        RGBA color;
        istringstream stream("#b0b #FaCe #CADD1E #DEADBEEF #BAdFAdmore_input_here");

        stream >> color;
        CHECK_EQUAL(0xbb, static_cast<int>(color.r));
        CHECK_EQUAL(0x00, static_cast<int>(color.g));
        CHECK_EQUAL(0xbb, static_cast<int>(color.b));
        CHECK_EQUAL(0x00, static_cast<int>(color.a));

        stream >> color;
        CHECK_EQUAL(0xff, static_cast<int>(color.r));
        CHECK_EQUAL(0xaa, static_cast<int>(color.g));
        CHECK_EQUAL(0xcc, static_cast<int>(color.b));
        CHECK_EQUAL(0xee, static_cast<int>(color.a));

        stream >> color;
        CHECK_EQUAL(0xca, static_cast<int>(color.r));
        CHECK_EQUAL(0xdd, static_cast<int>(color.g));
        CHECK_EQUAL(0x1e, static_cast<int>(color.b));
        CHECK_EQUAL(0x00, static_cast<int>(color.a));

        stream >> color;
        CHECK_EQUAL(0xde, static_cast<int>(color.r));
        CHECK_EQUAL(0xad, static_cast<int>(color.g));
        CHECK_EQUAL(0xbe, static_cast<int>(color.b));
        CHECK_EQUAL(0xef, static_cast<int>(color.a));

        stream >> color;
        CHECK_EQUAL(0xba, static_cast<int>(color.r));
        CHECK_EQUAL(0xdf, static_cast<int>(color.g));
        CHECK_EQUAL(0xad, static_cast<int>(color.b));
        CHECK_EQUAL(0x00, static_cast<int>(color.a));
        string token;
        stream >> token; // The fact that there was more input right after the color shouldn't affect us.
        CHECK_EQUAL("more_input_here", token);
    }

    TEST(TestMixColors)
    {
        // Mixing N colors yields the average of all channels.
        CHECK_EQUAL(RGBA("#7f007f40"), mix( { RGBA("#ff000080"), RGBA("#0000ff00") } ));
        CHECK_EQUAL(RGBA(0xff/4, 0xff/4, 0xff/4, 0xff/4),
                    mix( { RGBA("#f000"), RGBA("#0f00"), RGBA("#00f0"), RGBA("#000f") } ));

        // Mixing the same color N times yields that very color.
        CHECK_EQUAL(RGBA("#fff"),      mix( { RGBA("#fff"), RGBA("#fff"), RGBA("#fff") } ));

        // Mixing the empty set yields the default color (whatever it is.)
        CHECK_EQUAL(RGBA(),            mix( { } ));
    }


    TEST(TestColorCubes) {
        string color1 = "#ff0000";
        string color2 = "#0000ff80";
        Generator generator = Generator {
            "progname",
            // "-v",
            "-o /dev/null",
            "2",
            "1",
            "1",
            "AB",
            "-c A = " + color1 + ", B=" + color2
        };

        RGBA rgba1(color1), rgba2(color2);

        CHECK_EQUAL(rgba1, generator.color_map().at('A'));
        CHECK_EQUAL(rgba2, generator.color_map().at('B'));
        CHECK_EQUAL(2u, generator.color_map().size());

        // Did the cubes pick up the assigned colors to begin with?
        CHECK_EQUAL(rgba1, generator.cube_table()[0].color());
        CHECK_EQUAL(rgba2, generator.cube_table()[1].color());

        // There need to be exactly five faces with color rgba1, five with
        // color rgba2, one face whose color is an even mix, and *nothing
        // else*.
        int count1 = 0, count2 = 0, mix_count = 0;
        for (auto iter = generator.face_table().begin(); iter != generator.face_table().end(); ++iter) {
            const Face& face = *iter;
            if (face.color() == rgba1) {
                ++count1;
            } else if (face.color() == rgba2) {
                ++count2;
            } else if (face.color() == mix( { rgba1, rgba2 } )) {
                ++mix_count;
            } else {
                // No other face color should be possible.
                cout << "Unexpected face color \"" << face.color() << "\" encountered.\n";
                CHECK(false);
            }
        }
        CHECK_EQUAL(5, count1);
        CHECK_EQUAL(5, count2);
        CHECK_EQUAL(1, mix_count);

        // There need to be exactly eight edges with color rgba1, eight with
        // color rgba2, four edges whose color is an even mix, and *nothing
        // else*.
        count1 = count2 = mix_count = 0;
        for (auto iter = generator.edge_table().begin(); iter != generator.edge_table().end(); ++iter) {
            const Edge& edge = *iter;
            if (edge.color() == rgba1) {
                ++count1;
            } else if (edge.color() == rgba2) {
                ++count2;
            } else if (edge.color() == mix( { rgba1, rgba2 } )) {
                ++mix_count;
            } else {
                // No other edge color should be possible.
                cout << "Unexpected edge color \"" << edge.color() << "\" encountered.\n";
                CHECK(false);
            }
        }
        CHECK_EQUAL(8, count1);
        CHECK_EQUAL(8, count2);
        CHECK_EQUAL(4, mix_count);

        // There need to be exactly four vertices with color rgba1, four with
        // color rgba2, four whose color is an even mix, and *nothing else*.
        count1 = count2 = mix_count = 0;
        for (auto iter = generator.vertex_lattice().begin(); iter != generator.vertex_lattice().end(); ++iter) {
            const Vertex& vertex = *iter;

            // This vertex does not contribute, so it will not be colored.
            // This won't happen for the two-adjacent-cube test data I'm
            // using, but it's better to be safe and prepared.
            if (vertex.cubes().size() == 0) {
                continue;
            }

            if (vertex.color() == rgba1) {
                ++count1;
            } else if (vertex.color() == rgba2) {
                ++count2;
            } else if (vertex.color() == mix( { rgba1, rgba2 } )) {
                ++mix_count;
            } else {
                // No other vertex color should be possible.
                cout << "Unexpected vertex color \"" << vertex.color() << "\" encountered.\n";
                CHECK(false);
            }
        }
        CHECK_EQUAL(4, count1);
        CHECK_EQUAL(4, count2);
        CHECK_EQUAL(4, mix_count);
    }
}

// TODOs:
// * Allow specifying the dimensions as "WxHxD"?
// * Allow specifying the dimensions as "WxH", with D deduced using the
//   algorithm below?
// * Add const to the signature of all of the Generator member functions which
//   *ought* to be const.
// * Refactor: Make the SomaFileReader its own class.
// * Make everything properly unsigned (making depth(), width(), and
//   height() unsigned was silly.  Do you really want a depth of -1?)
//
// * Finish TestDeduceDimensionsWithTruncation().
// * If two out of the three dimensions are missing, maybe we should just
//   assume a linear data string and set the missing dimensions equal to 1.
// * Change the truncation algorithm in parse_data():
//   - If all three parameters (w, h, and d) are at non-default values, then
//     yes, truncate or pad as normal.  (Existing tests should pass after
//     implementing this.)
//   - If exactly ONE of the three parameters has a default value, deduce its
//     value from the other two values and the length of the data string.
//     Then round down and truncate as needed.
//     * Examples:
//       + width=4, height=4, data=",--.|AB||CD|`--'xxx": depth = floor(19/(4*4))=1.
//         Truncate three characters.
//       + width=4, height=4, data="@..@@..@@..@": depth = floor(12/16) = 0.
//         That's too small; ++depth and pad with 4*4*1-12=4 characters.
// * Add an option which causes the cubes to be separated in all three
//   dimensions by a fixed scalar value.
//   - Such an option is obviously mutually incompatible with optimization,
//     but it *ought* to be fairly easy in the non-optimized case (we just
//     adjust the coordinates as we emit the vertices.)
// * Right now, the SOMA format allows comments (and is generally better), but
//   is strictly limited to the colors of SOMAPLUS.  That is actually an
//   unexpected result; my parser was too good!
//   - You can get away with having more colors (read: having more unique
//     characters in the data string), but only if you use the "raw" format.
//     And even *that* is limited to US-ASCII at the moment.
//   - So either you get:
//     * The raw format, no comments, and up to 127-32 = 95 colors, or
//     * The SOMA format, comments, and less than 16 colors.
//
//   The first improvement is to add full Unicode support for the data string,
//   allowing us to use millions of colors in the "raw" format.  This means
//   representing the data string internally with UCS-4
//   (basic_string<wchar_t>.)
//
//   The second improvement, potentially, would be to provide a command-line
//   switch that disabled the strict color checking on SOMA input files.  This
//   would permit files that Thorleif's "Solver" program would reject, but we
//   could then convert (say) image files to SOMA files that included
//   information about the image in the comments.
int main()
{
    return UnitTest::RunAllTests();
}
