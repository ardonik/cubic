// =========================================================================
// Copyright (C) 2012 Ardonik <ardonik@ardonik.org>
//
// This file is part of ASC2OFF.
//
// ASC2OFF is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ASC2OFF is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ASC2OFF.  If not, see <http://www.gnu.org/licenses/>.
// =========================================================================

#include "cube.h"

using std::array;

Cube::Cube()
  : edges_(), vertices_(), faces_(), coordinate_(0, 0, 0),
    color_(default_cube_color) {
    // The edges are lazily inserted one-face-at-a-time instead of all being
    // inserted at once, so we need to have some means of keeping track of
    // "where we are" in terms of insertion.
    edges_.fill(-1);
}

Point Cube::coordinate() const { return coordinate_; }
void Cube::coordinate(const Point& p) { coordinate_ = p; }

const array<int, 8>& Cube::vertices() const { return vertices_; }
array<int, 8>& Cube::vertices() { return vertices_; }

const array<int, 12>& Cube::edges() const { return edges_; }
array<int, 12>& Cube::edges() { return edges_; }

const array<int, 6>& Cube::faces() const { return faces_; }
array<int, 6>& Cube::faces() { return faces_; }

const RGBA& Cube::color() const { return color_; }
RGBA& Cube::color() { return color_; }

int Cube::add_edge(int edge_index)
{
    for (size_t i = 0; i < edges_.size(); ++i) {
        if (edges_[i] < 0) {
            edges_[i] = edge_index;
            return i;
        }
    }
    return -1;
}
