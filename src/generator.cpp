// =========================================================================
// Copyright (C) 2012 Ardonik <ardonik@ardonik.org>
//
// This file is part of ASC2OFF.
//
// ASC2OFF is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ASC2OFF is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ASC2OFF.  If not, see <http://www.gnu.org/licenses/>.
// =========================================================================

#include "generator.h"
#include "utilities.h"
#include <stdexcept>
#include <string>
#include <algorithm>
#include <sstream>
#include <iostream>
#include <fstream>
#include <iterator>
#include <cstdlib> // NULL, atoi(), strtol()
#include <cerrno>  // errno
#include <vector>
#include <array>
#include <map>
#include <ctime>
#include <getopt.h>

using std::cin;
using std::map;
using std::max;
using std::atoi;
using std::cerr;
using std::cout;
using std::endl;
using std::exit;
using std::time;
using std::array;
using std::gmtime;
using std::string;
using std::strtol;
using std::time_t;
using std::vector;
using std::replace;
using std::asctime;
using std::istream;
using std::ostream;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::back_inserter;
using std::runtime_error;
using std::initializer_list;

const string project_name              = "ASC2OFF"; // Should really be a CMake variable
const string project_version           = "1.0";     // Should also be a CMake variable
const string project_copyright         = "Copyright (C) 2012 Ardonik <ardonik@ardonik.org>"; // This, too?

// Providing the defaults here means that I only have to change them in one
// place.
const int    default_width                  = 0;
const int    default_height                 = 0;
const int    default_depth                  = 0;
const bool   default_verbose                = false;
const string default_data                   = "";
const string default_name                   = "";
const string default_input_file             = "";
const string default_output_file            = "-";
const bool   default_remove_hidden_elements = false;
const string default_empty_characters       = "-. ";
const double default_scale_factor           = 1.0; // The distance in 3-space between two adjacent vertices.
const RGBA   default_face_color             = RGBA(128, 128, 128, 0);

// These are the SOMA puzzle colors that Thorleif Bundgaard's SOLVER program
// uses, so as far as I am concerned, they are official.
const array<RGBA, 11> standard_soma_colors = {
    {
        RGBA("#ff00ff"), // SOMA puzzle piece #1, "The V"
        RGBA("#ff0000"), // SOMA puzzle piece #2, "The L"
        RGBA("#ffff00"), // SOMA puzzle piece #3, "The T"
        RGBA("#0000ff"), // SOMA puzzle piece #4, "The Z"
        RGBA("#00ff00"), // SOMA puzzle piece #5, "The A"
        RGBA("#c0c0c0"), // SOMA puzzle piece #6, "The B"
        RGBA("#00ffff"), // SOMA puzzle piece #7, "The P"
        RGBA("#804000"), // SOMAplus puzzle piece #8, "The I"
        RGBA("#8000ff"), // SOMAplus puzzle piece #9, "The Q"
        RGBA("#ff8000"), // SOMAplus puzzle piece #10, "The S"
        RGBA("#408080"), // SOMAplus puzzle piece #0 (11 for us), "The D"
    }
};

// ===========================================================================
/// Returns true if a standard container contains this particular value.
///
/// All this does is save me a little typing.
template <typename T, typename V> inline bool contains(T container, const V& value)
{
    return find(container.begin(), container.end(), value) != container.end();
}

// ===========================================================================
// The object this constructor generates is totally useless!  This
// constructor's only real purpose is to make Generators easier to declare now
// and reassign later without having to play pointer games.
Generator::Generator()
  : width_(default_width), height_(default_height), depth_(default_depth),
    verbose_(default_verbose), data_(default_data),
    input_file(default_input_file), output_file(default_output_file),
    remove_hidden_elements(default_remove_hidden_elements),
    empty_characters(default_empty_characters) { }

// ===========================================================================
// The initializer list constructors are only here for the convenience of the
// automated tests.
Generator::Generator(const initializer_list<string>& list, istream& input, ostream& output)
  : width_(default_width), height_(default_height), depth_(default_depth),
    verbose_(default_verbose), data_(default_data),
    input_file(default_input_file), output_file(default_output_file),
    remove_hidden_elements(default_remove_hidden_elements),
    empty_characters(default_empty_characters) {

    vector<string> arguments;
    copy(list.begin(), list.end(), back_inserter(arguments));
    generate(arguments, input, output);
}

// ===========================================================================
// Provides cin and cout as "default arguments."  (You can't actually use
// "istream& input = std::cin" as a default argument in C++, so I needed a
// new constructor overload.)
Generator::Generator(const initializer_list<string>& list)
  : width_(default_width), height_(default_height), depth_(default_depth),
    verbose_(default_verbose), data_(default_data),
    input_file(default_input_file), output_file(default_output_file),
    remove_hidden_elements(default_remove_hidden_elements),
    empty_characters(default_empty_characters) {

    vector<string> arguments;
    copy(list.begin(), list.end(), back_inserter(arguments));
    generate(arguments, cin, cout);
}

Generator::Generator(int argc, const char* argv[], istream& input, ostream& output)
  : width_(default_width), height_(default_height), depth_(default_depth),
    verbose_(default_verbose), data_(default_data),
    input_file(default_input_file), output_file(default_output_file),
    remove_hidden_elements(default_remove_hidden_elements),
    empty_characters(default_empty_characters) {

    vector<string> arguments;
    auto begin = &argv[0];
    auto end = begin + argc;
    copy(begin, end, back_inserter(arguments));
    generate(arguments, input, output);
}

// ===========================================================================
// Provides cin and cout as "default arguments."  (You can't actually use
// "istream& input = std::cin" as a default argument in C++, so I needed a
// new constructor overload.)
Generator::Generator(int argc, const char* argv[])
  : width_(default_width), height_(default_height), depth_(default_depth),
    verbose_(default_verbose), data_(default_data),
    input_file(default_input_file), output_file(default_output_file),
    remove_hidden_elements(default_remove_hidden_elements),
    empty_characters(default_empty_characters) {

    vector<string> arguments;
    auto begin = &argv[0];
    auto end = begin + argc;
    copy(begin, end, back_inserter(arguments));
    generate(arguments, cin, cout);
}

// ===========================================================================
// This is for printing debug messages, so it keys off of the --verbose flag.
void Generator::print(const string& message) const {
    if (verbose_) {
        cerr << message << endl;
    }
}

void Generator::usage(const string& program_name, ostream& out) const {
    out << "\n"
        << "Generates a set of connected cubes in the OFF file format.\n\n"
        << "Usage: \n\n"
        << "(1) " << program_name << " [-i SOMAFILE] [optional flags]\n"
        << "(2) " << program_name << " [-i TEXTFILE] [-w WIDTH] [-h HEIGHT] [-d DEPTH] [optional flags]\n"
        << "(3) " << program_name << " [-D DATASTRING] [-w WIDTH] [-h HEIGHT] [-d DEPTH] [optional flags]\n"
        << "\n"
        << "SOMAFILE, if provided, must be in the SOLVER SOMA puzzle input file format.\n\n"
        << "Standard arguments:\n\n"
        << "  -w N,    --width=N      The width of the final object, in cubes.\n"
        << "  -h N,    --height=N     The height of the final object, in cubes.\n"
        << "  -d N,    --depth=N      The depth of the final object, in cubes.\n"
        << "  -D str,  --data=str     The data string in XZY order, one character per cube.\n"
        << "  -i file, --input=file   The data string input file.  Use '-' for stdin.\n"
        << "\n"
        << "Optional arguments:\n\n"
        << "  -o file, --output=file  The OFF output file.  Defaults to '-' (stdout).\n"
        << "  -c list, --colors=list  A comma-separated list of character=color pairs.\n"
        << "  -O,      --optimize     Removes internal shared faces, edges, and vertices.\n"
        << "  -v,      --verbose      Prints out more information about what's going on.\n"
        << "  -V,      --version      Prints program version and license information.\n"
        << "  -H,      --help         Prints this help message.\n"
        << "\n"
        << "If all three dimensions are provided, the data string will be truncated or\n"
        << "padded as necessary to meet the required number of cubes.  Either '-', '.',\n"
        << " or ' ' can be used to represent an empty cube.  If both -D and -i are\n"
        << "provided, the input file's contents will come first in the data string.\n\n"
        << "Examples:\n\n"
        << "(1) A 3x3x1 ring made of eight cubes: -w 3 -h 3 -d 1 ####-####\n"
        << "(2) The same as above, but colored half red and half transparent blue: -c\"A=#f00,B=#0000ff80\" -w 3 -h 3 -d 1 AAAB-ABBB\n"
        << "(3) A 5x5 square pyramid: -w 5 -h 3 -d 5 ............#..................###..###..###......#########################\n\n";
}

void Generator::display_version(ostream& out) const
{
    out << "\n"
        << project_name << ", version " << project_version << "\n"
        << project_copyright << "\n\n"
        << "This is free software, and you are welcome to redistribute it under certain\n"
        << "conditions.  This program comes with ABSOLUTELY NO WARRANTY; for details, see\n"
        << "the file `COPYING' in the source distribution.\n\n";
}

// ===========================================================================
// What the Generator does, in a nutshell.
void Generator::generate(const vector<string>& arguments, istream& input, ostream& output) {
    parse_command_line(arguments, input, output);
    parse_data();
    emit_off_file(output);
}

// ===========================================================================
// Uses the arguments from the command line to set up the Generator's
// data string, dimensions, and flags.
void Generator::parse_command_line(const vector<string>& arguments,
                                   istream& input_stream,
                                   ostream& output_stream) {

    // Force (GNU) getopt() to restart scanning argv from the beginning.  Not
    // doing this will cause subsequent getopt() calls on different argv[]
    // arrays to work improperly, or even crash.
    //
    // I can't emphasize this enough: not doing this will cause REALLY, REALLY
    // BAD BUGS.  Like getopt() mysteriously clobbering your data.  Like
    // segfaults on some inputs but not others.
    ::optind = 0;

    if (arguments.size() < 1) {
        // Shouldn't happen.
        throw runtime_error("Internal error: there must be at least one argument (the program name.)");
    }


    // Holds pointers to the actual character data for each argument.
    vector<vector<char>> argv_buffers;
    vector<char*> argv;

    // Convert the strings into an array of char const* that getopt() will
    // accept.
    for (unsigned int i = 0; i < arguments.size(); ++i) {
        const string& arg = arguments[i];
        argv_buffers.push_back(vector<char>());
        argv_buffers.back().resize(arg.size() + 1);
        copy(arg.begin(), arg.end(), argv_buffers.back().begin());
        argv_buffers.back()[arg.size()] = '\0';
        argv.push_back(&argv_buffers.back()[0]);
    }

    // Don't print an error message to stderr if the command line option is not
    // recognized as a supported getopt option.
    ::opterr = 0;

    // Specify --command-line-options.
    option long_options_table[] = {
        { "width",    required_argument, NULL, 'w' },
        { "height",   required_argument, NULL, 'h' },
        { "depth",    required_argument, NULL, 'd' },
        { "verbose",  no_argument,       NULL, 'v' },
        { "input",    required_argument, NULL, 'i' },
        { "output",   required_argument, NULL, 'o' },
        { "data",     required_argument, NULL, 'D' },
        { "color",    required_argument, NULL, 'c' },
        { "optimize", no_argument,       NULL, 'O' },
        { "version",  no_argument,       NULL, 'V' },
        { "help",     no_argument,       NULL, 'H' },
        { 0, 0, 0, 0 },
    };

    // Specify -c -l -o.
    //
    // The initial colon indicates that missing arguments for valid options
    // cause getopt to return ":" instead of "?".
    // A single colon suffix means "has a required argument."
    // A double-colon suffix means "has an optional argument."
    const char* short_options = ":w:h:d:vVHOo:D:c:i:";

    // Parse the command line with getopt().
    int status = -1, long_option_index = 0;
    stringstream stream;
    do {
        status = getopt_long(argv.size(), &argv[0], short_options, long_options_table, &long_option_index);

        switch (status) {
            case -1: // Getopt scanning complete.
                break;
            case 'H': // --help, -H
                usage(argv[0], output_stream);
                exit(0);
            case 'V': // --version, -V
                display_version(output_stream);
                exit(0);
            case 'w': // --width=N, -w N
                width_ = atoi(::optarg);
                break;
            case 'h': // --height=N, -h N
                height_ = atoi(::optarg);
                break;
            case 'd': // --depth=N, -d N
                depth_ = atoi(::optarg);
                break;
            case 'v': // --verbose, -v
                verbose_ = true;
                break;
            case 'o': // --output=file, -o file
                output_file = trim(::optarg);
                break;
            case 'i': // --input=file, -i file
                input_file = trim(::optarg);
                if (input_file != "-") {
                    ifstream input_file_stream(input_file);
                    if (!input_file_stream) {
                        throw runtime_error("Input file \"" + input_file + "\" does not exist.");
                    }
                    name_ = input_file;
                }
                break;
            case 'D': // --data=string, -D string
                data_ = trim(::optarg);
                break;
            case 'c': // --color=list, -c list
                parse_color_list(::optarg);
                break;
            case 'O': // --optimize, -O
                remove_hidden_elements = true;
                break;
            case ':': { // Missing required argument for supported option
                stream.str("");
                stream << "The option \"-" << (char)::optopt << "\" is missing a required argument.";
                throw runtime_error(stream.str());
            }
            case '?': { // Unrecognized short option character

                // Bloody undocumented behavior.  According to the getopt()
                // documentation, ::optopt should give me the actual
                // unrecognized option here:
                //
                //     If getopt() finds an option character in argv that was
                //     not included in optstring, or if it detects a missing
                //     option argument, it returns '?'  and sets the external
                //     variable optopt to the actual option character.
                //
                // For unrecognized *short* options, this is indeed the case.
                // But for unrecognized *long* options, ::optopt always has a
                // value of '\0' instead.
                //
                // The actual bad argument in that case is stored in the *old*
                // (not current) value of ::optind.  How did I find that out?
                // Via the web: http://stackoverflow.com/a/2725408

                stream.str("");
                if (::optopt != 0) {
                    if (::optopt == 'H' || ::optopt == 'v' || ::optopt == 'V' || ::optopt == 'O') {
                        // More undocumented behavior!  ::optopt is also set
                        // if unnecessary arguments are supplied.
                        stream << "Option \"-" << (char)::optopt << "\" does not take an argument.";
                    } else {
                        stream << "Unrecognized short option \"-" << (char)::optopt << "\".";
                    }
                } else {
                    // Works on GNU getopt even with argument reordering.
                    // Not sure if this will work on POSIXLY_CORRECT or
                    // non-GNU versions.
                    stream << "Unrecognized long option \"" << argv[::optind - 1] << "\".";
                }
                string message = stream.str();
                throw runtime_error(message);
            }
            default: {
                // Unrecognized option?
                stringstream stream;
                stream << "Entered default case because getopt() returned '"
                       << (char)status << "' (" << status
                       << ").  This was not expected.";
                print(stream.str());
                break;
            }
        }
    } while (status != -1);

    // At this point, any unrecognized options are at the end of argv, starting
    // at ::optind.
    stream.str("");
    stream << "Leftover arguments: ";
    for (unsigned int i = ::optind; i < argv.size(); ++i) {
        stream << "\"" << argv[i] << "\"";

        // What sort of string is this unrecognized argument?
        char* endptr;
        errno = 0;
        int value = strtol(argv[i], &endptr, 10);
        if (ifstream(argv[i])) {

            // A filename is an unusual, but valid, choice of data string.  Do you want -i
            // or not?
            data_ += argv[i];
            stream << " (filename will be treated as a data string; did you mean to use --input='" << argv[i] << "' instead?)";

        } else if (errno != 0 || endptr == argv[i]) {

            // Integer parse error.  Append this argument to the data string.
            data_ += argv[i];
            stream << " (not an integer -- adding to data string)";

        } else {

            // If the unrecognized argument is an integer, it becomes:
            // * The width if no width is set
            // * The height if the width is set but the height is not
            // * The depth if the width and height are set but the depth is not

            if (width_ == default_width) {
                width_ = value;
                stream << " (width)";
            } else if (height_ == default_height) {
                height_ = value;
                stream << " (height)";
            } else if (depth_ == default_depth) {
                depth_ = value;
                stream << " (depth)";
            } else {
                // Unnecessary integer detected!  Ignore it, but tell them.
                stream << " (erroneous integer argument -- ignored)";
            }
        }
        stream << ", ";
    }
    print(stream.str());

    data_ = process_data_string(input_stream, data_, input_file);

    if (width_ == default_width || height_ == default_height || depth_ == default_depth) {
        print("parse_command_line: Warning: Cannot deduce the width, height, or depth from the data string alone.  Maybe you should pass all three of those values in as command-line arguments.");
    }
}

// ===========================================================================
// Read the data string from standard input or a file if such was supplied to
// us from the command line.
//
// The algorithm this function executes is as follows:
//
//   Let p, the preliminary data string, equal the combined_data_string_argument.
//   Let f, the final data string, equal "".
//   If input_file_argument != "" Then
//     p = [contents of input file or input_stream] + p
//   End If
//   If p starts with "/SOMA" Then
//     Read p as if it were a SOMA file
//     If read successful Then
//       Set this->color_map_ using SOMA file values
//       Set this->width_, this->height_, and this->depth_ using SOMA file values
//       Add all "digit characters" and "empty characters" to f
//     Else
//       Error: "Invalid SOMA file"
//     End If
//   Else
//     For each input line in p Do
//       Trim input line of padding whitespace characters
//       f += line
//     End For
//     If length(f) > this->width_ * this->height_ * this->depth_ Then
//       Truncate f
//     Else If length(f) < this->width_ * this->height_ * this->depth_ Then
//       Add empty padding characters to f
//     End If
//   End If
//
//   Return f
string Generator::process_data_string(istream& input_stream,
                                      const string& combined_data_string_argument,
                                      const string& input_file_argument) {

    string result;
    string data_string = combined_data_string_argument;
    stringstream debug_stream;

    // If there is an input file or standard input stream, read it and
    // *prepend* it to whatever the current value of the data_string is.
    // Prepending allows allows the data argument(s) to supplement--not
    // override--the "base data string" coming from standard input.
    //
    // I expect that in most cases, people will supply data from standard
    // input *or* the data arguments, but not both.  Still, we support the
    // operation and make it do the right thing.

    if (input_file != default_input_file) {
        debug_stream.str("process_data_string: ");
        debug_stream << "Data string size is currently " << data_.size()
                     << ".  Reading supplemental data from ";

        ifstream input_file_stream;
        istream& in = (input_file_argument == "-" ? input_stream : input_file_stream);
        if (input_file_argument != "-") {
            input_file_stream.open(input_file_argument);
            debug_stream << "\"" << input_file_argument << "\"";
        } else {
            debug_stream << "standard input";
        }

        // Suck the rest of the input (regardless of where it came from) into
        // a single gigantic string, preserving whitespace, newlines, and
        // everything.
        //
        // TODO: Can't we do this with an istream_iterator or something of
        // that sort?  We don't really need to read the input line by line--we
        // just need to read all of the rest of the input, whitespace and all.
        string input_stream_string;
        while (in) {
            string line;
            getline(in, line);
            input_stream_string += (line + "\n");
        }

        // The input stream comes first!  The data string from the args comes
        // second.
        data_string = input_stream_string + data_string;

        debug_stream << "... read " << input_stream_string.size() << " character(s).";
        print(debug_stream.str());
    }

    // At this point, the data string is as large as it's ever going to be.
    // If it's a SOMA file, parse it like that.
    stringstream data_string_stream(data_string);
    string soma_file_data_string;
    if (read_soma_file(data_string_stream, soma_file_data_string)) {

        result = soma_file_data_string;

    } else {

        // Otherwise, just read it line by line and trim each line of excess
        // whitespace.

        print("process_data_string: Reading non-SOMA input.");
        while (data_string_stream) {
            string line;
            getline(data_string_stream, line);
            result += trim(line);
            print("process_data_string: Read line: \"" + trim(line) + "\"");
        }

        // Now that we have the complete data string, see if we can figure out
        // what our real dimensions are (unless, of course, the user already
        // supplied us with the real dimensions.)
        deduce_dimensions(width_, height_, depth_, result);

        // If the data string at this point is too long or not long enough,
        // truncate it or add padding (respectively.)

        size_t expected_size = static_cast<unsigned int>(width_ * height_ * depth_);
        if (result.size() < expected_size) {

            debug_stream.str("process_data_string: ");
            debug_stream << "Padding data string with " << expected_size - result.size()
                         << " extra characters.";
            print(debug_stream.str());

            result += string(expected_size - result.size(), ' ');

        } else if (result.size() > expected_size) {

            debug_stream.str("process_data_string: ");
            debug_stream << "Truncating " << result.size() - expected_size
                         << " excess character(s) from the end of the data string.";
            print(debug_stream.str());

            result = result.substr(0, expected_size);
        }
    }

    debug_stream.str("");
    debug_stream << "process_data_string: Final data string in XZY order:\n";
    for (int offset = 0, y = 0; y < height_; ++y) {
        debug_stream << "process_data_string:\n";
        for (int z = 0; z < depth_; ++z) {
            debug_stream << "process_data_string: ";
            for (int x = 0; x < width_; ++x, ++offset) {
                debug_stream.put(result[offset]);
                // debug_stream << (int)result[offset] << ' ';
            }
            debug_stream.put('\n');
        }
    }
    string s = debug_stream.str();
    print(s.substr(0, s.size() - 1)); // Chop off the last newline.

    return result;
}

// ===========================================================================
// Attempts to guess a missing width, height, or depth.  Note that deduction
// will not succeed if more than one of the three dimensions is missing.
void Generator::deduce_dimensions(int& width, int& height, int& depth, const string& data) {

    bool width_provided  = (width > 0  && width  != default_width);
    bool height_provided = (height > 0 && height != default_height);
    bool depth_provided  = (depth > 0  && depth  != default_depth);

    stringstream debug_stream;
    debug_stream << std::boolalpha;

    debug_stream.str("deduce_dimensions: ");
    debug_stream << "Width provided: " << width_provided << " (" << width << ")";
    print(debug_stream.str());

    debug_stream.str("deduce_dimensions: ");
    debug_stream << "Height provided: " << height_provided << " (" << height << ")";
    print(debug_stream.str());

    debug_stream.str("deduce_dimensions: ");
    debug_stream << "Depth provided: " << depth_provided << " (" << depth << ")";
    print(debug_stream.str());

    if (width_provided && height_provided && depth_provided) {

        print("deduce_dimensions: nothing to deduce.");

    } else if (!width_provided && height_provided && depth_provided) {

        width = max(static_cast<int>(data.length() / (height * depth)), 1);

        debug_stream.str("deduce_dimensions: ");
        debug_stream << "Deduced width = "
                     << data.length() << "/(" << height << " * " << depth
                     << ") -> " << width;
        print(debug_stream.str());

    } else if (width_provided && !height_provided && depth_provided) {

        height = max(static_cast<int>(data.length() / (width * depth)), 1);

        debug_stream.str("deduce_dimensions: ");
        debug_stream << "Deduced height = "
                     << data.length() << "/(" << width << " * " << depth
                     << ") -> " << height;
        print(debug_stream.str());

    } else if (width_provided && height_provided && !depth_provided) {

        depth  = max(static_cast<int>(data.length() / (width * height)), 1);

        debug_stream.str("deduce_dimensions: ");
        debug_stream << "Deduced depth = "
                     << data.length() << "/(" << width << " * " << height
                     << ") -> " << depth;
        print(debug_stream.str());

    } else {

        // Two or more dimensions were not provided.
        throw runtime_error("deduce_dimensions: Insufficient information to determine dimensions.  Please supply at least two of the three dimension parameters (width, height, and depth.)");
    }
}


// ===========================================================================
// Parses the comma-separated list of RGBA colors that comes after "-c" or
// "--color=".
void Generator::parse_color_list(const std::string& color_list) {

    vector<string> data = split(color_list, ",");

    for (auto iter = data.begin(); iter != data.end(); ++iter) {

        // Each of these is supposed to be a "value=color" pair.
        stringstream stream;
        bool error = false;

        size_t index = iter->find("=");
        if (index == string::npos) {

            stream << "No '=' found in list token \"" << *iter << "\".";
            error = true;

        } else {

            stream.str("");
            stream << "In assignment \"" << *iter << "\": ";

            string key = trim(iter->substr(0, index));
            string value = trim(iter->substr(index)); // This includes the "=" character.

            if (key.size() > 1) {

                stream << "\"" << key << "\" is not a valid single character.";
                error = true;

            } else {

                stream.clear();
                stream.str(value.substr(1)); // Skip that "=" character.
                RGBA color;
                stream >> color;

                if (!stream) {

                    stream << "\"" << value.substr(1)
                           << "\" is not a valid RGB or RGBA color.";
                    error = true;

                } else {

                    color_map_[key[0]] = color;

                }
            } // end (if the key was valid)
        } // end (if an '=' was found)

        if (error) {
            string error_message = stream.str();
            stream.str("");
            stream << "parse_color_list: " << error_message << "  Color assignments must be in the form c=#rgb, c=#rrggbb, c=#rgba, or c=#rrggbbaa.";
            throw runtime_error(stream.str());
        }
    } // end (for each token in the comma-separated list)
}

// ===========================================================================
// If the input looks like a SOMA file, reads it and adjusts the dimension
// variables of the generator accordingly.
bool Generator::read_soma_file(istream& input_stream, string& result)
{
    istream::off_type original_position = input_stream.tellg();
    stringstream stream;

    // The "standard," such as it is, can be found here:
    // http://www.fam-bundgaard.dk/SOMA/FIGURES/NOTATION.HTM
    do {
        string first_line;
        getline(input_stream, first_line);

        if (!input_stream) {
            stream.str("");
            stream << "read_soma_file: End of input after \"" << first_line
                   << "\".  Input data is not a SOMA file.";
            print(stream.str());
            break;
        }

        if (first_line.length() < 5 || first_line.substr(0, 5) != "/SOMA") {
            stream.str("");
            stream << "read_soma_file: The first five characters of the string \""
                   << first_line << "\" are not \"/SOMA\".  Input data is not a SOMA file.";
            print(stream.str());
            break;
        }

        name_ = first_line.substr(1);
        print("read_soma_file: SOMA input file detected.  Figure name: " + name_);

        // From this point on, we assume that the input *is* a valid SOMA
        // file, and we'll throw an exception if we don't like what we see!
        result = "";

        // Read the remaining lines.
        vector<vector<string>> row_records;
        string first_record;
        size_t number_of_records_per_line = 0;
        int line_number = 2; // Already read the header, remember?

        do {
            string line;
            getline(input_stream, line);
            line = trim(line);

            // Just in case a problem occurs.
            stream.str("");
            stream << "read_soma_file: Error on line " << line_number << " (\"" << line
                   << "\"): ";
            string error_message_prefix = stream.str();

            // Delete the comment portion, if any.
            //
            // As far as I can tell from reading the Soma Figures page, there are
            // additional constraints on comments:
            //
            //   * Comments must start on the first column
            //   * Comments are only valid between the header and the first data line
            //
            // I do not enforce any of these restrictions, because I'm not sure if
            // those are actual rules or mere convention.

            size_t semicolon_index = line.find(';');
            if (semicolon_index != string::npos) {
                stream.str("");
                stream << "read_soma_file: Comment: " << line.substr(semicolon_index);
                print(stream.str());
                line = line.substr(0, semicolon_index);
            }

            // From the spec: "A figure description starts with /"
            if (line.size() > 0 && line[0] == '/') {

                if (line == "/") {

                    stream.str("");
                    stream << error_message_prefix
                           << "Figure description line contains no cubes or empty space.";
                    throw runtime_error(stream.str());

                } else {

                    // Removing the initial (and, if necessary, final) '/'
                    // characters ensures that we won't have empty start and
                    // end tokens after the split.
                    //
                    // By "records", I refer refer to the parts of figure
                    // description lines which lie between the "/" characters.
                    string string_to_split;
                    if (*line.rend() == '/') {
                        string_to_split = line.substr(1, line.size() - 2);
                    } else {
                        string_to_split = line.substr(1);
                    }
                    vector<string> records = split(string_to_split, "/");

                    if (first_record.size() == 0) {

                        // Set the standard that all other figure description
                        // lines will have to meet.
                        first_record = records[0];
                        number_of_records_per_line = records.size();

                    } else if (records.size() != number_of_records_per_line) {

                        stream.str("");
                        stream << error_message_prefix
                               << "Record count mismatch.  All figure description lines are expected to contain "
                               << number_of_records_per_line
                               << "records, but this line contains "
                               << records.size()
                               << "records.";
                        throw runtime_error(stream.str());
                    }

                    // Verify that all records have the same length as the
                    // first record in the file.
                    for (auto iter = records.begin(); iter != records.end(); ++iter) {
                        if (iter->size() != first_record.size()) {

                            stream.str("");
                            stream << error_message_prefix
                                   << "Record length mismatch.  The first record in this SOMA file has length "
                                   << first_record.size()
                                   << ", but a record on this figure description line has length "
                                   << iter->size()
                                   << ".  All records in the SOMA file must have the same length.";
                            throw runtime_error(stream.str());

                        } else { // A valid record.  Finally!

                            // Use a little string substitution to make the
                            // colors easier to map.  This isn't a crucial
                            // part of the operation.

                            // From the spec: "Cubes are named 1,2,3,4,5,6,7
                            // or V,L,T,Z,A,B,P"
                            replace(iter->begin(), iter->end(), 'V', '1');
                            replace(iter->begin(), iter->end(), 'L', '2');
                            replace(iter->begin(), iter->end(), 'T', '3');
                            replace(iter->begin(), iter->end(), 'Z', '4');
                            replace(iter->begin(), iter->end(), 'A', '5');
                            replace(iter->begin(), iter->end(), 'B', '6');
                            replace(iter->begin(), iter->end(), 'P', '7');

                            // From the spec: "When we talk about the SOMAplus
                            // pieces I call them 0,1,2,3,4,5,6,7,8,9,$ or
                            // D,V,L,T,Z,A,B,P,I,Q,S"
                            replace(iter->begin(), iter->end(), 'I', '8');
                            replace(iter->begin(), iter->end(), 'Q', '9');
                            replace(iter->begin(), iter->end(), 'S', '$');
                            replace(iter->begin(), iter->end(), 'D', '@');

                            // From the spec: "Empty cells are written as 0 or
                            // . or -"
                            //
                            // For the record, I don't like those 0s.  They
                            // look too much like valid data, and in SOMAplus,
                            // they ARE valid data.
                            //
                            // Fortunately, the online SOMAplus figures on
                            // Thorleif's SOMAplus page always use letters.
                            replace(iter->begin(), iter->end(), '0', '.');

                            // From the spec: "NO other lines are allowed
                            // inside a figure description."
                            //
                            // (Note that this makes using custom colors
                            // outside of the SOMAplus defaults impossible.
                            // If you want fine-grained color control with
                            // more than 11 shades, then you don't want the
                            // SOMA format.)

                            size_t invalid_character_index = iter->find_first_not_of("1234567.-89$@");
                            if (invalid_character_index != string::npos) {

                                stream.str("");
                                stream << error_message_prefix
                                       << "A record on the current figure description line contains an invalid character: '"
                                       << iter->at(invalid_character_index)
                                       << "'.";
                                throw runtime_error(stream.str());
                            }

                            stream.str("");
                            stream << "read_soma_file: Added record: \"" << *iter << "\"";
                            print(stream.str());
                        }
                    } // end (for each record in the current figure description line)

                    row_records.push_back(records);

                } // end (if the current line looks like a valid figure description)

                // The only other constraint I can see from the spec is this:
                //
                // "There MUST be an empty line after a figure."
                //
                // I don't know what this means, so I'm not enforcing it.

            } else {

                // From the spec: "Any text may be written in all the other
                // lines."
                stream.str("");
                stream << "read_soma_file: Ignoring extra text: \"" << line << "\"";
                print(stream.str());
            }

            ++line_number;
        } while (input_stream);

        // If control made it here, we read the SOMA input file successfully
        // and it is now stored in row_records.
        //
        // Set the dimensions.
        size_t new_width = first_record.length();
        size_t new_height = number_of_records_per_line;
        size_t new_depth = row_records.size();

        stream.str("");
        if (width_ != default_width) {
            stream << "read_soma_file: SOMA file width (" << new_width
                   << ") overrides existing width (" << width_ << ").";
        } else {
            stream << "read_soma_file: SOMA file width appears to be "
                   << new_width << ".";
        }
        print(stream.str());

        stream.str("");
        if (height_ != default_height) {
            stream << "read_soma_file: SOMA file height (" << new_height
                   << ") overrides existing height (" << height_ << ").";
        } else {
            stream << "read_soma_file: SOMA file height appears to be "
                   << new_height << ".";
        }
        print(stream.str());

        stream.str("");
        if (depth_ != default_depth) {
            stream << "read_soma_file: SOMA file depth (" << new_depth
                   << ") overrides existing depth (" << depth_ << ").";
        } else {
            stream << "read_soma_file: SOMA file depth appears to be "
                   << new_depth << ".";
        }
        print(stream.str());

        width_ = new_width;
        height_ = new_height;
        depth_ = new_depth;

        // Create the actual string.
        //
        // Note that SOMA files store their input *records* in YZ order (and
        // hence store their total character data in XYZ order); this class
        // stores its data in XZY order.
        //
        // To get the data string to look right, we effectively have to read
        // the SOMA file from the bottom to the top!
        for (int y = 0; y < height_; ++y) {
            for (int z = depth_ - 1; z >= 0; --z) {
                result += row_records[z][y];
            }
        }

        // Assign the color palette, but ONLY if it hasn't already been set.
        for (size_t index = 0; index < standard_soma_colors.size(); ++index) {
            char c = '0' + index + 1;
            if (index == 9) c = '$';
            if (index == 10) c = '@';
            if (color_map_.find(c) == color_map_.end()) {
                stream.str("");
                stream << "read_soma_file: Added standard SOMA figure color mapping: '"
                       << c << "' = (" << standard_soma_colors[index] << ")";
                print(stream.str());
                color_map_[c] = standard_soma_colors[index];
            } else {
                stream.str("");
                stream << "read_soma_file: Can't add standard SOMA figure color mapping '"
                       << c << "' = (" << standard_soma_colors[index]
                       << ") because that character is already mapped to ("
                       << color_map_[c] << ").";
                print(stream.str());
            }

        }

        stream.str("");
        stream << "read_soma_file: Success.  Data string length = " << result.size() << ".";
        print(stream.str());
        return true;

    } while (false);

    // If control made it here, an error was encountered.  Reset the stream so
    // that it can be read from by the normal input routine.
    input_stream.seekg(original_position);
    input_stream.clear();
    return false;
}

// ===========================================================================
// Transforms the data string into cubes.
void Generator::parse_data() {

    /// Fill the vertex lattice.
    vertex_lattice_.resize((width_ + 1) * (height_ + 1) * (depth_ + 1));
    for (size_t offset = 0; offset < vertex_lattice_.size(); ++offset) {
        Point p = coordinate(offset, width_ + 1, height_ + 1, depth_ + 1);
        vertex_lattice_[offset].x = p.x;
        vertex_lattice_[offset].y = p.y;
        vertex_lattice_[offset].z = p.z;
    }

    /// Now that all of the grunt work is taken care of, read the data string
    /// and fill the cube lattice.
    ///
    /// The data string is given in "XZY" order.  What does that mean?  It
    /// means that for width = height = depth = 3, a data string like this:
    ///
    ///   ####-#####-#---#-#####-####
    ///
    /// is really divided into three 3x1x3 layers parallel to the XZ plane:
    ///
    ///     y=0        y=1        y=2
    ///     ---------  ---------  ---------
    ///     ####-####  #-#---#-#  ####-####
    ///
    /// and those layers are in turn divided into 3x1x1 rows parallel to the X
    /// axis:
    ///
    ///         y=0   |   y=1   |   y=2
    ///         ------+---------+------
    ///               |         |
    ///    z=2  ###   |   #-#   |   ###
    ///    z=1  #-#   |   ---   |   #-#
    ///    z=0  ###   |   #-#   |   ###
    ///               |         |
    ///       x=012   | x=012   | x=012
    ///
    /// In short, the string is divided into rows starting from the top front,
    /// proceeding to the top back, and then continuing to the next plane
    /// below.  There are width() cubes per row, depth() rows per plane, and
    /// height() planes.
    ///
    /// In all this, I assume a left-handed system where the positive Z axis
    /// goes into the screen and the X and Y axes are aligned with the
    /// viewer's face (with the positive X axis pointed right and the positive
    /// Y axis pointed up.)
    for (int y = 0, data_index = 0; y < height_; ++y) {
        for (int z = 0; z < depth_; ++z) {
            for (int x = 0; x < width_; ++x, ++data_index) {
                Point p(x, y, z);

                // Is this cube even "on" to begin with?
                char current_data_string_character = data_[data_index];
                if (empty_characters.find(current_data_string_character) !=
                    string::npos) {
                    // There's an "empty" character in this position, so don't
                    // emit a cube.
                    continue;
                }

                // While we're here, give it a color if such was requested.
                RGBA cube_color = default_cube_color;
                auto iter = color_map_.find(current_data_string_character);
                if (iter != color_map_.end()) {
                    cube_color = iter->second;
                }

                add_cube(p, cube_color);
            }
        }
    }

    // Now that the faces are registered, use their vertices to create the
    // edges for every cube at the same time.
    add_edges();

    // Use the cube colors to give each face, edge, and vertex a color.
    assign_face_colors(cube_table_, face_table_);
    assign_edge_colors(cube_table_, edge_table_);
    assign_vertex_colors(cube_table_, vertex_lattice_);
}

// ===========================================================================
// Adds a single cube and its associated faces to the Generator.
void Generator::add_cube(Point p, const RGBA& cube_color) {
    Cube cube;
    cube.coordinate(p);
    cube.color() = cube_color;

    // This cube "owns" eight vertices.
    //
    // The (x, y, z) coordinate of the front upper left vertex is
    // the same as the coordinate of the cube itself; they just
    // live in lattices of different sizes.  A trivial conversion
    // takes care of the offset calculation, and then from there,
    // the relative offsets to the other vertices are constant.

    int v = this->offset(p, width_ + 1, height_ + 1, depth_ + 1);

    int front_upper_left_vertex  = v;
    int front_upper_right_vertex = v + 1;
    int front_lower_left_vertex  = v + (width_ + 1);
    int front_lower_right_vertex = v + (width_ + 1) + 1;
    int rear_upper_left_vertex   = v + (width_ + 1) * (height_ + 1);
    int rear_upper_right_vertex  = v + (width_ + 1) * (height_ + 1) + 1;
    int rear_lower_left_vertex   = v + (width_ + 1) * (height_ + 1) + (width_ + 1);
    int rear_lower_right_vertex  = v + (width_ + 1) * (height_ + 1) + (width_ + 1) + 1;

    cube.vertices()[0] = front_upper_left_vertex;
    cube.vertices()[1] = front_upper_right_vertex;
    cube.vertices()[2] = front_lower_left_vertex;
    cube.vertices()[3] = front_lower_right_vertex;
    cube.vertices()[4] = rear_upper_left_vertex;
    cube.vertices()[5] = rear_upper_right_vertex;
    cube.vertices()[6] = rear_lower_left_vertex;
    cube.vertices()[7] = rear_lower_right_vertex;

    cube_table_.push_back(cube);
    int cube_index = cube_table_.size() - 1;

    // Tell each of the vertices that we're one of their owners.
    for (int i = 0; i < 8; ++i) {
        vertex_lattice_[cube.vertices()[i]].cubes().push_back(cube_index);
    }

    // Now that the cube is registered, use its vertices to create the cube's
    // six faces.
    add_faces(cube_index,
              front_upper_left_vertex,
              front_upper_right_vertex,
              front_lower_left_vertex,
              front_lower_right_vertex,
              rear_upper_left_vertex,
              rear_upper_right_vertex,
              rear_lower_left_vertex,
              rear_lower_right_vertex);
}

void Generator::add_faces(int cube_index,
                          int front_upper_left_vertex,
                          int front_upper_right_vertex,
                          int front_lower_left_vertex,
                          int front_lower_right_vertex,
                          int rear_upper_left_vertex,
                          int rear_upper_right_vertex,
                          int rear_lower_left_vertex,
                          int rear_lower_right_vertex) {

    // There are up to six *potential* new faces to add, but we're forced to
    // check for duplicates here.  This table contains all of the potential
    // faces' vertices.
    //
    // The order of these indices actually matters because Antiprism will
    // display a face in black if its surface normal points away from the
    // viewer!  The vertices for each face must be COUNTERCLOCKWISE from the
    // perspective of an observer standing on the outside of the cube just in
    // front of the face.
    vector<vector<int>> face_vertex_indices = vector<vector<int>> {
        { front_lower_left_vertex,  front_lower_right_vertex, front_upper_right_vertex, front_upper_left_vertex },  // Front
        { rear_upper_left_vertex,   rear_upper_right_vertex,  rear_lower_right_vertex,  rear_lower_left_vertex },   // Back
        { front_upper_left_vertex,  rear_upper_left_vertex,   rear_lower_left_vertex,   front_lower_left_vertex },  // Left
        { front_lower_right_vertex, rear_lower_right_vertex,  rear_upper_right_vertex,  front_upper_right_vertex }, // Right
        { front_upper_left_vertex,  front_upper_right_vertex, rear_upper_right_vertex,  rear_upper_left_vertex },   // Top
        { rear_lower_left_vertex,   rear_lower_right_vertex,  front_lower_right_vertex, front_lower_left_vertex },  // Bottom
    };

    array<string, 6> adjectives = {
        { "front", "back", "left", "right", "top", "bottom" }
    }; // For displaying verbose messages

    for (int i = 0; i < 6; ++i) {
        Face face;
        face.vertices()[0] = face_vertex_indices[i][0];
        face.vertices()[1] = face_vertex_indices[i][1];
        face.vertices()[2] = face_vertex_indices[i][2];
        face.vertices()[3] = face_vertex_indices[i][3];
        face.cubes().push_back(cube_index);

        int face_index = find_duplicate_face(face);
        stringstream stream;
        if (face_index >= 0) {
            // Matching face already exists; the current cube has a neighbor!
            stream << "add_faces(): the " << adjectives[i] << " face for Cube #"
                   << cube_index << " already exists in face_table["
                   << face_index << "].  Adding the cube as a new parent.";
            print(stream.str());
            cube_table_[cube_index].faces()[i] = face_index;             // Use the existing face.
            face_table_[face_index].cubes().push_back(cube_index);       // This cube must be a parent, too!
        } else {
            // Never seen this particular combination of vertices before.
            stream << "add_faces(): adding Face #" << face_table_.size()
                   << " as the " << adjectives[i] << " face for Cube #"
                   << cube_index << " with vertex indices { "
                   << face.vertices()[0] << ", " << face.vertices()[1] << ", "
                   << face.vertices()[2] << ", " << face.vertices()[3] << " }.";
            print(stream.str());
            face_table_.push_back(face);                                 // Add the new face to the system.
            cube_table_[cube_index].faces()[i] = face_table_.size() - 1; // Use the new face.
        }
    }
}

// ===========================================================================
// Goes through the entire face_table() and adds four Edge objects to
// each Face.
void Generator::add_edges() {

    for (auto iter = face_table_.begin(); iter != face_table_.end(); ++iter) {
        const Face& face = *iter;

        for (int vertex_index = 0; vertex_index < 4; ++vertex_index) {

            int current_vertex_index = vertex_index, next_vertex_index = (vertex_index + 1) % 4;
            Edge temp;
            temp.vertices()[0] = face.vertices()[current_vertex_index];
            temp.vertices()[1] = face.vertices()[next_vertex_index];

            int existing_edge_index = find_duplicate_edge(temp);

            stringstream stream;
            stream << "Face #" << (iter - face_table_.begin()) << ": The edge "
                   << temp << " is "
                   << (existing_edge_index < 0 ? "unknown" : "already known");
            print(stream.str());

            int edge_index = -1;
            if (existing_edge_index < 0) {
                // Add the new edge to the system.
                edge_table_.push_back(temp);
                edge_index = edge_table_.size() - 1;
            } else {
                // Use the edge that's already there.
                edge_index = existing_edge_index;
            }
            Edge& edge = edge_table_[edge_index];

            // Whether or not this edge was new, it must "inherit" any cubes
            // that already own this face.  Sure, the current face can be
            // owned by two cubes at most, but as other faces are processed,
            // the edge will get a more full set of parents.
            for (auto iter = face.cubes().begin(); iter != face.cubes().end(); ++iter) {

                if (!contains(edge.cubes(), *iter)) {

                    // Any parent cubes that our face owns, this edge must own
                    // also.
                    edge.cubes().push_back(*iter);
                    stream.str("");
                    stream << "  Adding Cube #" << *iter << " to this edge.";
                    print(stream.str());

                    // If this edge didn't know about the cube, the cube
                    // wouldn't have known about this edge either.  Insert
                    // it.
                    Cube& current_cube = cube_table_[*iter];
                    if (current_cube.add_edge(edge_index) < 0) {

                        // The cube already had 12 edges prior to insertion.
                        // This is a pretty good indication that *someone*
                        // screwed up somewhere, because we already checked
                        // for duplicates!  This edge has never been in the
                        // system before, so how could any of our cubes be
                        // full?
                        //
                        // I think this is serious enough to warrant halting
                        // execution to fix my logic, though it could easily
                        // be fixed by putting a duplicate insertion check in
                        // Cube::add_edge() (and thus glossing over any flaws
                        // in my parsing algorithm!)
                        stringstream error_stream;
                        error_stream << "Internal error: edge_table_[" << edge_index
                                     << "] is supposed to be new, but one of the cubes that own it (cube_table_["
                                     << *iter
                                     << "]) already has 12 edges.  If the cube is truly this edge's parent, then that situation is not possible.";
                        throw runtime_error(error_stream.str());
                    } // end (if an impossible situation occurred)

                } else {

                    stream.str("");
                    stream << "  This edge already contains Cube #" << *iter
                           << " (and " << (edge.cubes().size() - 1) << " other cubes.)";
                    print(stream.str());

                } // end (if this edge already contains the current cube)
            } // end (for each cube "owning" the current face)
        } // end (for each vertex pair in the current face)
    } // end (for each face)
}

// ===========================================================================
// Have we seen this Face before?
int Generator::find_duplicate_face(const Face& proposed_face) const {
    // I can't think of a more clever technique here than a plain old linear
    // search.
    for (size_t i = 0; i < face_table_.size(); ++i) {
        if (proposed_face.has_same_vertices(face_table_[i])) {
            return i;
        }
    }
    return -1;
}

// ===========================================================================
// Have we seen this Edge before?
int Generator::find_duplicate_edge(const Edge& proposed_edge) const {
    // Same thing as before.
    for (size_t i = 0; i < edge_table_.size(); ++i) {
        if (proposed_edge.has_same_vertices(edge_table_[i])) {
            return i;
        }
    }
    return -1;
}

// ===========================================================================
// Remove unused cubes (and hidden faces, if needed) from the Generator, then
// return some smaller, fully-cross-referenced data structures reflecting that
// change.
void Generator::create_compact_tables(vector<Vertex>& vertex_table,
                                      vector<Edge>& edge_table,
                                      vector<Face>& face_table,
                                      vector<Cube>& cube_table) const
{
    vertex_table.clear();
    edge_table.clear();
    face_table.clear();
    cube_table.clear();
    stringstream stream;

    // This associative array maps indices in the vertex_lattice_ to indices
    // in the new vertex_table.
    map<int, int> vertex_index_map;

    for (size_t i = 0; i < vertex_lattice_.size(); ++i) {

        const Vertex& current_vertex = vertex_lattice_[i];

        // An "unused" vertex, for the purposes of this program, is one which
        // does not have a cube assigned to it.
        bool vertex_in_use = (current_vertex.cubes().size() > 0);

        // A vertex is hidden if it is completely surrounded by cubes.
        bool vertex_is_hidden = (remove_hidden_elements && current_vertex.cubes().size() == 8);

        if (vertex_in_use && !vertex_is_hidden) {
            vertex_table.push_back(current_vertex);
            vertex_index_map[i] = vertex_table.size() - 1;
        }
    }

    stream.str("");
    stream << "Compacting: Eliminated "
           << vertex_lattice_.size() - vertex_table.size()
           << " redundant vertices; " << vertex_table.size()
           << " vertices remain.";
    print(stream.str());

    // Remap the faces.
    for (auto face_iter = face_table_.begin(); face_iter != face_table_.end(); ++face_iter) {

        stream.str("");
        if (remove_hidden_elements && face_iter->cubes().size() == 2) {

            // This face is hidden by the cubes that surround it.  Let's just
            // not insert it.
            //
            // Yes, that means that some of the cube_table.faces() will be
            // invalid, but fortunately, the connectivity of those data
            // structures are no longer needed at this point for OFF file
            // generation.  (Compare that to the vertices, which *are* needed
            // for OFF file generation and therefore have to be renumbered.)

            stream << "Compacting: Eliminating face " << *face_iter
                   << " because it is hidden by cubes #"
                   << face_iter->cubes()[0] << " and #"
                   << face_iter->cubes()[1] << ".";
            print(stream.str());


        } else {

            Face face = *face_iter;
            stream << "Compacting: Mapped face " << face << " to ";
            face.vertices()[0] = vertex_index_map[face.vertices()[0]];
            face.vertices()[1] = vertex_index_map[face.vertices()[1]];
            face.vertices()[2] = vertex_index_map[face.vertices()[2]];
            face.vertices()[3] = vertex_index_map[face.vertices()[3]];
            stream << face;
            print(stream.str());

            face_table.push_back(face);
        }
    }

    // Remap the edges.
    for (auto edge_iter = edge_table_.begin(); edge_iter != edge_table_.end(); ++edge_iter) {

        stream.str("");
        if (remove_hidden_elements && edge_iter->cubes().size() == 4) {

            // This edge is hidden by the cubes that surround it.  Let's just
            // not insert it.
            stream << "Compacting: Eliminating edge " << *edge_iter
                   << " because it is hidden by cubes #"
                   << edge_iter->cubes()[0] << ", #"
                   << edge_iter->cubes()[1] << ", #"
                   << edge_iter->cubes()[2] << ", and #"
                   << edge_iter->cubes()[3] << ".";
            print(stream.str());

        } else {

            Edge edge = *edge_iter;
            stream << "Compacting: Mapped edge " << edge << " to ";
            edge.vertices()[0] = vertex_index_map[edge.vertices()[0]];
            edge.vertices()[1] = vertex_index_map[edge.vertices()[1]];
            stream << edge;
            print(stream.str());

            edge_table.push_back(edge);
        }
    }

    // Remap the cubes.
    //
    // TODO: These aren't used for OFF file generation, so I don't see this
    // part of the operation as being necessary.  Will eliminate later.
    for (auto cube_iter = cube_table_.begin(); cube_iter != cube_table_.end(); ++cube_iter) {

        bool has_six_neighbors =
            (face_table_.at(cube_iter->faces()[0]).cubes().size() == 2 &&
             face_table_.at(cube_iter->faces()[1]).cubes().size() == 2 &&
             face_table_.at(cube_iter->faces()[2]).cubes().size() == 2 &&
             face_table_.at(cube_iter->faces()[3]).cubes().size() == 2 &&
             face_table_.at(cube_iter->faces()[4]).cubes().size() == 2 &&
             face_table_.at(cube_iter->faces()[5]).cubes().size() == 2);

        if (remove_hidden_elements && has_six_neighbors) {

            // This cube is hidden by the cubes that surround it.  Let's just
            // not insert it.
            stream << "Compacting: Eliminating cube #" << (cube_iter - cube_table_.begin())
                   << " because it is hidden by six neighboring cubes.";
            print(stream.str());

        } else {
            Cube cube = *cube_iter;
            cube.vertices()[0] = vertex_index_map[cube.vertices()[0]];
            cube.vertices()[1] = vertex_index_map[cube.vertices()[1]];
            cube.vertices()[2] = vertex_index_map[cube.vertices()[2]];
            cube.vertices()[3] = vertex_index_map[cube.vertices()[3]];
            cube.vertices()[4] = vertex_index_map[cube.vertices()[4]];
            cube.vertices()[5] = vertex_index_map[cube.vertices()[5]];
            cube.vertices()[6] = vertex_index_map[cube.vertices()[6]];
            cube.vertices()[7] = vertex_index_map[cube.vertices()[7]];

            cube_table.push_back(cube);
        }
    }
}

// ===========================================================================
// Give each Face of each Cube a color which is a mix of all the adjacent
// Cubes' colors.
void Generator::assign_face_colors(const vector<Cube>& cube_table, vector<Face>& face_table)
{
    for (auto face_iter = face_table.begin(); face_iter != face_table.end(); ++face_iter) {
        Face& face = *face_iter;

        // At most, there will be only two parent cubes per face.
        vector<RGBA> face_colors;
        for (auto cube_iter = face.cubes().begin(); cube_iter != face.cubes().end(); ++cube_iter) {
            face_colors.push_back(cube_table[*cube_iter].color());
        }

        // The average of all the bordering cubes' colors becomes our own.
        face.color() = mix(face_colors.begin(), face_colors.end());
    }
}

// ===========================================================================
// Give each Edge of each Cube a color which is a mix of all the adjacent
// Cubes' colors.
void Generator::assign_edge_colors(const std::vector<Cube>& cube_table, std::vector<Edge>& edge_table) {

    for (auto edge_iter = edge_table.begin(); edge_iter != edge_table.end(); ++edge_iter) {
        Edge& edge = *edge_iter;

        // At most, there will be only four parent cubes per edge.
        vector<RGBA> edge_colors;
        for (auto cube_iter = edge.cubes().begin(); cube_iter != edge.cubes().end(); ++cube_iter) {
            edge_colors.push_back(cube_table[*cube_iter].color());
        }

        // The average of all the bordering cubes' colors becomes our own.
        edge.color() = mix(edge_colors.begin(), edge_colors.end());
    }
}

// ===========================================================================
// Give each Vertex of each Cube a color which is a mix of all the adjacent
// Cubes' colors.
void Generator::assign_vertex_colors(const std::vector<Cube>& cube_table, std::vector<Vertex>& vertex_table)
{
    for (auto vertex_iter = vertex_table.begin(); vertex_iter != vertex_table.end(); ++vertex_iter) {
        Vertex& vertex = *vertex_iter;

        // At most, there will be only eight parent cubes per vertex.
        vector<RGBA> vertex_colors;
        for (auto cube_iter = vertex.cubes().begin(); cube_iter != vertex.cubes().end(); ++cube_iter) {
            vertex_colors.push_back(cube_table[*cube_iter].color());
        }

        // The average of all the bordering cubes' colors becomes our own.
        vertex.color() = mix(vertex_colors.begin(), vertex_colors.end());
    }
}

// ===========================================================================
// Last but not least, the function which is the main point of both this class
// and this entire project.
//
// Oh, look.  There's official documentation at
// http://www.antiprism.com/programs/off_format.html.  AT the time I wrote
// this method, I was unaware of that page.
void Generator::emit_off_file(ostream& output_stream)
{
    vector<Vertex> vertex_table;
    vector<Edge> edge_table;
    vector<Face> face_table;
    vector<Cube> cube_table;
    create_compact_tables(vertex_table, edge_table, face_table, cube_table);

    // The distance in 3-space between two adjacent vertices.
    const double scale_factor = default_scale_factor;

    // Determine where the OFF file will be written.
    ofstream file;
    if (output_file != "-") {
        file.open(output_file);
    }
    ostream& out = (output_file == "-" ? output_stream : file);

    // A fancy string for the generation time.
    string timestamp;
    array<char, 100> buffer;
    time_t generation_time = time(NULL);
    if (strftime(buffer.data(), buffer.size(), "%FT%TZ", gmtime(&generation_time)) != 0) {
        timestamp = buffer.data();
    } else {
        timestamp  = asctime(gmtime(&generation_time)); // Not RFC 3339-compliant.  Oh, well.
    }

    // The header and prelude.
    out << "OFF\n";
    if (output_file == "-") {
        if (name_ != "") {
            out << "# " << name_ << "\n";
        }
    } else {
        out << "# " + output_file + "\n";
    }
    out << "# Generated on " << timestamp << " by " << project_name
        << ", version " << project_version << ".\n";

    // Write out the indices.
    //
    // Antiprism is a strange beast.  Instead of expecting vertices, polygons,
    // and edges like one would expect for an indexed_poly file, it converts
    // every edge into a two-vertex, degenerate "polygon", converts every
    // vertex into a one-vertex, degenerate "polygon", and then reports in the
    // header that there are no actual edges in the OFF file.
    //
    // Is that standard?  Paul Bourke's document makes no mention of such
    // behavior (I suppose the standard *allows* for it, though, provided that
    // degenerate polygons are handled properly.)  My guess is that Antiprism
    // does it so that they can assign colors to each vertex and edge, since
    // polygons in OFF files accept colors and edges/vertices do not.
    ///
    // Note that Antiprism leaves the edges out of its generated OFF files
    // unless someone explicitly colors them in with off_color or the like.
    // *This* program, on the other hand, always colors its edges, so they are
    // always included in the count.
    out << "#\n"
        << "# Indices: TotalVertices TotalPolygons+TotalEdges+TotalVertices 0\n#\n"
        << vertex_table.size() << " "
        << face_table.size() + edge_table.size() + vertex_table.size()
        << " 0\n#\n";

    // Write out the actual vertex coordinates.
    out << "# Vertices (" << vertex_table.size() << "): x y z\n#\n";
    for (size_t i = 0; i < vertex_table.size(); ++i) {

        // Convert each point to a real three-space coordinate by scaling it.
        //
        // TODO: Should we also translate these so the lattice is centered at
        // the origin?
        double x = vertex_table[i].x * scale_factor;
        double y = vertex_table[i].y * scale_factor;
        double z = vertex_table[i].z * scale_factor;
        out << x << " " << y << " " << z << "\n";
    }

    out << "#\n# Faces (" << face_table.size() << "): NumVertices=4 Vertex_1 Vertex_2 Vertex_3 Vertex_4 Red Green Blue Alpha\n#\n";
    for (size_t i = 0; i < face_table.size(); ++i) {
        const Face& face = face_table[i];
        RGBA color = face.color();
        color.a = 255 - color.a;
        out << "4 " << face.vertices()[0] << " " << face.vertices()[1]
            << " " << face.vertices()[2] << " " << face.vertices()[3]
            << " " << color << "\n";
    }

    // Write out the edges as degenerate polygons.
    out << "#\n# Edges (" << edge_table.size() << "): NumVertices=2 Vertex_1 Vertex_2 Red Green Blue Alpha\n#\n";
    for (size_t i = 0; i < edge_table.size(); ++i) {
        const Edge& edge = edge_table[i];
        RGBA color = edge.color();
        color.a = 255 - color.a;
        out << "2 " << edge.vertices()[0] << " " << edge.vertices()[1]
            << " " << color << "\n";
    }

    // Write out the vertices as degenerate polygons, too.
    out << "#\n# Vertices (" << vertex_table.size() << "): NumVertices=1 Vertex_1 Red Green Blue Alpha\n#\n";
    for (size_t i = 0; i < vertex_table.size(); ++i) {
        const Vertex& vertex = vertex_table[i];
        RGBA color = vertex.color();
        color.a = 255 - color.a;
        out << "1 " << i << " " << color << "\n";
    }
}
