// =========================================================================
// Copyright (C) 2012 Ardonik <ardonik@ardonik.org>
//
// This file is part of ASC2OFF.
//
// ASC2OFF is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ASC2OFF is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ASC2OFF.  If not, see <http://www.gnu.org/licenses/>.
// =========================================================================

#include "rgba.h"
#include <sstream>
#include <cctype>
#include <iostream>
#include <iomanip>
#include <stdexcept>

using std::ios;
using std::hex;
using std::cerr;    // For debugging.
using std::setw;
using std::fixed;
using std::string;
using std::isspace;
using std::istream;
using std::ostream;
using std::isxdigit;
using std::stringstream;
using std::runtime_error;
using std::setprecision;

RGBA::RGBA(const std::string& color) : r(0), g(0), b(0), a(0) {
    stringstream stream(color);
    stream >> *this;
    if (!stream) {
        throw runtime_error("The token \"" + color + "\" is not a valid color.");
    }
}


bool RGBA::operator==(const RGBA& color) const {
    return (r == color.r && g == color.g && b == color.b && a == color.a);
}

bool RGBA::operator<(const RGBA& color) const {
    // This ordering is not intended to achieve any sort of visual effect
    // (like making less bright colors sort to lower values--that would call
    // for the standard luminance formula.)
    //
    // I just need a value that is guaranteed to be unique for any unique
    // RGBA.
    long my_value    =       (r << 24) |       (g << 16) |       (b << 8) |       a;
    long their_value = (color.r << 24) | (color.g << 16) | (color.b << 8) | color.a;
    return (my_value < their_value);
}


ostream& operator<< (ostream& out, const RGBA& rgba) {
    return out << setprecision(5) << fixed
               << rgba.r/255.0 << " " << rgba.g/255.0 << " "
               << rgba.b/255.0 << " " << rgba.a/255.0;
}

istream& operator>> (istream& in, RGBA& rgba) {

    if (!in) {
        cerr << "operator>>(istream, rgb): Nothing to read.\n";
        return in;
    }

    // Read in a prelude character and as many hexadecimal characters as we
    // can.
    string s;
    do {
        char c = in.peek();

        if (isspace(c)) { // Eat whitespace.

            in.get();

        } else if ((s.empty() && c == '#') ||                    // The initial '#'.
                   (!s.empty() && s[0] == '#' && isxdigit(c))) { // A hex digit after the initial '#'.

            s += in.get();

        } else { // Stop parsing if we see anything else.

            break;
        }
    } while (in);

    // Insufficient input.
    bool success = false;
    if (s.empty()) {
        cerr << "operator>>(istream, rgb): Error: Could not obtain any characters from the input stream.\n";
    } else if (s[0] != '#') {
        cerr << "operator>>(istream, rgb): Error: \"" << s << "\" did not begin with '#'.\n";
    } else if (s.size() < 4 || s.size() == 6 || s.size() == 8 || s.size() > 9) {
        cerr << "operator>>(istream, rgb): \"" << s << "\" must have 3, 4, 6, or 8 hex digits.\n";
    } else {

        // Only valid cases remain.
        stringstream stream(s);
        stream.get(); // Eliminate the '#'.

        // I'm not sure why stream>>hex>>setw(1)>>rgba.r doesn't work.  You'd
        // think that would tell the stream to read a single hex digit from the
        // input stream and convert that value into an integer, but instead, it
        // reads as many hexadecimal digits from the stream as it can (effectively
        // ignoring the setw() directive.)
        //
        // Because of that nonsense, I'm forced to read the entire hexadecimal
        // string at once and convert it the old-fashioned way.
        unsigned long value = 0;
        stream >> hex >> value;
        int r = 0, g = 0, b = 0, a = 0;

        if (s.size() == 4) { // #rgb
            r = (value >> 8) & 0xf;
            g = (value >> 4) & 0xf;
            b = (value >> 0) & 0xf;
            rgba.r = (r << 4) | r; // Duplicate the single hex digits.
            rgba.g = (g << 4) | g;
            rgba.b = (b << 4) | b;
            rgba.a = 0x00;
            success = true;
        } else if (s.size() == 5) { // #rgba
            r = (value >> 12) & 0xf;
            g = (value >> 8)  & 0xf;
            b = (value >> 4)  & 0xf;
            a = (value >> 0)  & 0xf;
            rgba.r = (r << 4) | r; // Duplicate the single hex digits.
            rgba.g = (g << 4) | g;
            rgba.b = (b << 4) | b;
            rgba.a = (a << 4) | a;
            success = true;
        } else if (s.size() == 7) { // #rrggbb
            rgba.r = (value >> 16) & 0xff;
            rgba.g = (value >> 8)  & 0xff;
            rgba.b = (value >> 0)  & 0xff;
            rgba.a = 0x00;
            success = true;
        } else if (s.size() == 9) { // #rrggbbaa
            rgba.r = (value >> 24) & 0xff;
            rgba.g = (value >> 16) & 0xff;
            rgba.b = (value >> 8)  & 0xff;
            rgba.a = (value >> 0)  & 0xff;
            success = true;
        } else {
            cerr << "operator>>(istream, rgb): Internal error: \"" << s << "\" should have had a valid value by now.\n";
        }

    } // end (if the input was valid)

    if (!success) {
        in.setstate(ios::failbit);
    }
    return in;
}

RGBA mix(const std::initializer_list<RGBA>& color_list) {
    return mix(color_list.begin(), color_list.end());
}
