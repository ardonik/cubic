// =========================================================================
// Copyright (C) 2012 Ardonik <ardonik@ardonik.org>
//
// This file is part of ASC2OFF.
//
// ASC2OFF is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ASC2OFF is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ASC2OFF.  If not, see <http://www.gnu.org/licenses/>.
// =========================================================================

#include "generator.h"
#include <stdexcept>
#include <iostream>

using namespace std;

void display_brief_help_message(const char* const program_name);

/// Since the generator does all the work, this program is trivial.
int main(int argc, const char* argv[])
{
    try {

        if (argc == 1) {
            display_brief_help_message(argv[0]);
            return 0;
        }

        Generator generator(argc, argv);

    } catch (const exception& e) {

        cerr << e.what() << "\n";
        display_brief_help_message(argv[0]);
        return 1;

    } catch (...) {

        cerr << "An unknown problem occurred.  (This should not normally happen.)\n";
        return 2;
    }

    return 0;
}

void display_brief_help_message(const char* const program_name) {
    cerr << "Type '" << program_name << " --help' for help.\n";
}
