// =========================================================================
// Copyright (C) 2012 Ardonik <ardonik@ardonik.org>
//
// This file is part of ASC2OFF.
//
// ASC2OFF is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ASC2OFF is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ASC2OFF.  If not, see <http://www.gnu.org/licenses/>.
// =========================================================================

#include "utilities.h"

using namespace std;

// Trims leading and trailing whitespace from the given input string.
string trim(const string& s)
{
    // This is a particularly common solution.
    string str = s;
    const string whitespace = " \t\r\n\b\v";
    str.erase(0, str.find_first_not_of(whitespace)); // Remove leading whitespace.
    str.erase(str.find_last_not_of(whitespace) + 1); // Remove trailing whitespace.
    return str;
}

// Splits a string into an array of tokens based on the given delimiter
// character(s).
vector<string> split(const string& s, const string& delimiters) {
    vector<string> result;
    size_t start = 0, end = start;

    do {
        end = s.find_first_of(delimiters, start);
        if (end == string::npos) {
            // There is still one final token after the last delimiter.
            //
            // Always.
            //
            // Even if the last delimiter is the last character of the
            // string (in which case, said token will simply be the empty
            // string.)
            result.push_back(s.substr(start));
            break;
        }
        result.push_back(s.substr(start, end - start));
        start = end + 1;
    } while (true);

    return result;
}
