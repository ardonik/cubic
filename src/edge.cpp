// =========================================================================
// Copyright (C) 2012 Ardonik <ardonik@ardonik.org>
//
// This file is part of ASC2OFF.
//
// ASC2OFF is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ASC2OFF is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ASC2OFF.  If not, see <http://www.gnu.org/licenses/>.
// =========================================================================

#include "edge.h"

using std::ostream;

bool Edge::has_same_vertices(const Edge& edge) const {
    return ((vertices_[0] == edge.vertices_[0] && vertices_[1] == edge.vertices_[1]) ||
            (vertices_[0] == edge.vertices_[1] && vertices_[1] == edge.vertices_[0]));
}

ostream& operator<< (ostream& out, const Edge& edge)
{
    return out << "{ " << edge.vertices_[0] << ", " << edge.vertices_[1] << " }";
}
