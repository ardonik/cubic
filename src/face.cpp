// =========================================================================
// Copyright (C) 2012 Ardonik <ardonik@ardonik.org>
//
// This file is part of ASC2OFF.
//
// ASC2OFF is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ASC2OFF is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ASC2OFF.  If not, see <http://www.gnu.org/licenses/>.
// =========================================================================

#include "face.h"

#include <algorithm>
#include <iostream>

using std::find;
using std::ostream;

ostream& operator<< (ostream& out, const Face& face) {
    return out << "{ " << face.vertices_[0]
               << ", " << face.vertices_[1]
               << ", " << face.vertices_[2]
               << ", " << face.vertices_[3] << " }";
}


bool Face::has_same_vertices(const Face& face) const {
    // As usual, I can't think of a more clever algorithm here than a linear
    // search.  Of course, since there will always be exactly sixteen
    // comparisons, the linear search can be done in O(1).
    return find(vertices_.begin(), vertices_.end(), face.vertices_[0]) != vertices_.end() &&
           find(vertices_.begin(), vertices_.end(), face.vertices_[1]) != vertices_.end() &&
           find(vertices_.begin(), vertices_.end(), face.vertices_[2]) != vertices_.end() &&
           find(vertices_.begin(), vertices_.end(), face.vertices_[3]) != vertices_.end();
}
