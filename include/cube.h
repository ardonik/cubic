// =========================================================================
// Copyright (C) 2012 Ardonik <ardonik@ardonik.org>
//
// This file is part of ASC2OFF.
//
// ASC2OFF is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ASC2OFF is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ASC2OFF.  If not, see <http://www.gnu.org/licenses/>.
// =========================================================================

#ifndef CUBE_H_INCLUDED__
#define CUBE_H_INCLUDED__

#include <array>
#include "point.h"
#include "rgba.h"

const RGBA default_cube_color = RGBA(0x80, 0x80, 0x80, 0); // A boring gray.

/// Represents any one of the cubes in the generated lattice.
/// This is merely a record-keeping data structure; it is not useful by
/// itself.
class Cube
{
    public:
        /// Constructs an empty, invalid cube.
        Cube();

        /// The position of the cube within the lattice.
        Point coordinate() const;
        void coordinate(const Point& p);

        /// Every cube "owns" eight vertices.  These are their offsets in the
        /// lattice table.
        const std::array<int, 8>& vertices() const;
        std::array<int, 8>& vertices();

        /// Every cube "owns" eight edges.  These are their offsets in the edge
        /// table.
        const std::array<int, 12>& edges() const;
        std::array<int, 12>& edges();

        /// Every cube "owns" six faces.  These are their offsets in the face
        /// table.
        const std::array<int, 6>& faces() const;
        std::array<int, 6>& faces();

        /// Every cube has a color; the faces inherit this color when the OFF
        /// file is generated.
        const RGBA& color() const;
        RGBA& color();

        /// Finds a slot in the edges() array where the given edge (index) can be
        /// added.
        ///
        /// @param edge_index The "edge" to add.  As far as the Cube class is
        ///                   concerned, an integer *is* an edge.
        /// @return The index within edges() where the edge was added, or -1 if
        ///         edge() was "full" (i.e., already had 12 insertions prior to
        ///         the call.)
        int add_edge(int edge_index);

    private:
        std::array<int, 12> edges_;
        std::array<int, 8> vertices_;
        std::array<int, 6> faces_;
        Point coordinate_;
        RGBA color_;
};


#endif // #ifndef CUBE_H_INCLUDED__
