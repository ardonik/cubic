// =========================================================================
// Copyright (C) 2012 Ardonik <ardonik@ardonik.org>
//
// This file is part of ASC2OFF.
//
// ASC2OFF is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ASC2OFF is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ASC2OFF.  If not, see <http://www.gnu.org/licenses/>.
// =========================================================================

#ifndef RGBA_H_INCLUDED__
#define RGBA_H_INCLUDED__

#include <string>
#include <initializer_list>
#include <iostream> // For debugging.

struct RGBA {
    public:
        /// Red.
        unsigned char r;

        /// Green.
        unsigned char g;

        /// Blue;
        unsigned char b;

        /// Alpha channel.  0 = fully opaque, 255 = fully transparent.
        unsigned char a;

        RGBA(unsigned char r_, unsigned char g_, unsigned char b_, unsigned char a_)
            : r(r_), g(g_), b(b_), a(a_) { }

        /// Fully opaque black.
        RGBA() : r(0), g(0), b(0), a(0) { }

        /// Read a string using a compact hexadecimal format.
        ///
        /// An exception will be thrown if the string is invalid.
        RGBA(const std::string& color);

        /// Compare two colors for equality.
        bool operator==(const RGBA& color) const;

        /// Compare two colors for ordering.
        ///
        /// This is only provided so that RGBA objects can be used as keys in
        /// std::maps.
        bool operator<(const RGBA& color) const;

        /// Outputs normalized colors in the format expected by OFF files.
        ///
        /// Half-transparent blue would be "0 0 1 0.5" (give or take
        /// any applicable floating-point rounding errors.)
        ///
        /// Antiprism uses five digits of precision for colors, so we do, too.
        friend std::ostream& operator<< (std::ostream& out, const RGBA& rgba);

        /// Reads RGBA colors in a compact hexadecimal format.
        ///
        /// Half-transparent blue would be "#0000FF80" under this scheme.
        /// Except for the extra alpha channel, HTML and CSS also use this
        /// color format.
        friend std::istream& operator>> (std::istream& in, RGBA& rgba);
};


/// Takes the average of all of the given colors' channels to create a
/// final, mixed color.
template<typename Iterator>
RGBA mix(Iterator begin, Iterator end) {
    unsigned int r = 0, g = 0, b = 0, a = 0, count = 0;
    for (Iterator iter = begin; iter != end; ++iter, ++count) {
        RGBA color = *iter;
        r += color.r;
        g += color.g;
        b += color.b;
        a += color.a;
    }

    return (count == 0 ? RGBA() : RGBA(r/count, g/count, b/count, a/count));
}

RGBA mix(const std::initializer_list<RGBA>& color_list);

#endif // (#ifndef RGBA_H_INCLUDED__)
