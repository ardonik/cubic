// =========================================================================
// Copyright (C) 2012 Ardonik <ardonik@ardonik.org>
//
// This file is part of ASC2OFF.
//
// ASC2OFF is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ASC2OFF is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ASC2OFF.  If not, see <http://www.gnu.org/licenses/>.
// =========================================================================

#ifndef UTILITIES_H_INCLUDED__
#define UTILITIES_H_INCLUDED__

#include <string>
#include <vector>

/// This file contains miscellaneous utilities that don't fit elsewhere.

/// Trims leading and trailing whitespace from the given input string.
std::string trim(const std::string& s);

/// Splits a string into an array of tokens based on the given delimiter
/// character(s).
std::vector<std::string> split(const std::string& s, const std::string& delimiters);

#endif // (#ifndef UTILITIES_H_INCLUDED__)
