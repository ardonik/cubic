// =========================================================================
// Copyright (C) 2012 Ardonik <ardonik@ardonik.org>
//
// This file is part of ASC2OFF.
//
// ASC2OFF is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ASC2OFF is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ASC2OFF.  If not, see <http://www.gnu.org/licenses/>.
// =========================================================================

#ifndef VERTEX_H_INCLUDED__
#define VERTEX_H_INCLUDED__

#include <vector>
#include "rgba.h"

/// A vertex is just a point with some membership information attached for
/// cross-referencing.
class Vertex : public Point
{
    public:
        Vertex() : Point(), cubes_(), color_()  { }

        /// This vertex can "belong" to up to eight cubes.  This array
        /// stores their offsets in the cube lattice.
        const std::vector<int>& cubes() const { return cubes_; }
        std::vector<int>& cubes() { return cubes_; }

        /// Every vertex is associated with a color that is derived from the
        /// cubes that it "belongs" to.
        const RGBA& color() const { return color_; }
        RGBA& color() { return color_; }

    private:
        std::vector<int> cubes_;
        RGBA color_;
};


#endif // #ifndef VERTEX_H_INCLUDED__
