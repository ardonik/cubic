// =========================================================================
// Copyright (C) 2012 Ardonik <ardonik@ardonik.org>
//
// This file is part of ASC2OFF.
//
// ASC2OFF is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ASC2OFF is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ASC2OFF.  If not, see <http://www.gnu.org/licenses/>.
// =========================================================================

#ifndef POINT_H_INCLUDED__
#define POINT_H_INCLUDED__

#include <iostream>

/// A point in space, or the coordinate of a cube in a lattice.
struct Point
{
public:
    Point() : x(0), y(0), z(0) { }
    Point(int x_, int y_, int z_) : x(x_), y(y_), z(z_) { }

    /// Are these two Points equal?
    bool operator==(const Point& p) const;

    // Prints a Point for debugging purposes.
    friend std::ostream& operator<< (std::ostream& out, const Point& p);

    /// Position in the left-right dimension.
    int x;

    /// Position in the up-down dimension.
    int y;

    /// Position in the near-far dimension.
    int z;

    /// Allows inheritance from this class to Do The Right Thing.
    virtual ~Point() { }
};


#endif // #ifndef POINT_H_INCLUDED__
