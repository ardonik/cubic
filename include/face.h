// =========================================================================
// Copyright (C) 2012 Ardonik <ardonik@ardonik.org>
//
// This file is part of ASC2OFF.
//
// ASC2OFF is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ASC2OFF is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ASC2OFF.  If not, see <http://www.gnu.org/licenses/>.
// =========================================================================

#ifndef FACE_H_INCLUDED__
#define FACE_H_INCLUDED__

#include <vector>
#include <array>
#include "rgba.h"

/// A face is just an array of four points with some membership information
/// attached for cross-referencing.
class Face
{
    public:
        Face() : cubes_(), vertices_(), color_() { }

        /// This face can "belong" to up to two cubes.  This array
        /// stores their offsets in the cube lattice.
        const std::vector<int>& cubes() const { return cubes_; }
        std::vector<int>& cubes() { return cubes_; }

        /// This face "owns" exactly four vertices.  These are their offsets
        /// in the vertex lattice.
        ///
        /// Note that ownership can be shared with other faces from
        /// neighboring cubes.
        std::array<int, 4>& vertices() { return vertices_; }
        const std::array<int, 4>& vertices() const { return vertices_; }

        /// Returns true if and only if all four vertex indices belonging to
        /// the given face also belong to us.
        bool has_same_vertices(const Face& face) const;

        /// Every face has a color that is inherited from the parent cubes.
        /// (That's right, I said cubes, plural.  A face can have up to two
        /// parents.)
        const RGBA& color() const { return color_; }
        RGBA& color() { return color_; }

        /// Print a face's vertex indices.
        friend std::ostream& operator<< (std::ostream& out, const Face& face);

    private:
        std::vector<int> cubes_;
        std::array<int, 4> vertices_;
        RGBA color_;
};


#endif // #ifndef FACE_H_INCLUDED__
