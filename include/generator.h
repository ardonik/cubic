// =========================================================================
// Copyright (C) 2012 Ardonik <ardonik@ardonik.org>
//
// This file is part of ASC2OFF.
//
// ASC2OFF is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ASC2OFF is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ASC2OFF.  If not, see <http://www.gnu.org/licenses/>.
// =========================================================================

#ifndef GENERATOR_H_INCLUDED__
#define GENERATOR_H_INCLUDED__

#include <string>
#include <vector>
#include <map>
#include <initializer_list>
#include <iostream>
#include "point.h"
#include "vertex.h"
#include "face.h"
#include "edge.h"
#include "cube.h"
#include "rgba.h"

/// Takes the given input and tries to generate OFF file data for it.
class Generator
{
  public:
    /// Creates a default generator that can't actually generate anything.
    Generator();

    /// This is a convenience function for passing in arguments without the
    /// rigmarole of converting to argc/argv format.
    ///
    /// @arguments: An initializer list of command-line arguments.  The
    ///             program name MUST be the first argument, as would be the
    ///             case for a real command line.  Example:
    ///             { "MyProgramName", "-w 5", "-v" }.
    /// @input:     The input stream that the generator will obtain its data
    ///             string from if ordered to read from standard input
    ///             ("--input=-").
    /// @output:    The output stream that the generator will write to by
    ///             default unless overridden by the arguments ("--output").
    Generator(const std::initializer_list<std::string>& arguments,
              std::istream& input,
              std::ostream& output);

    /// Does the same thing as a call to Generator(arguments, cin, cout).
    Generator(const std::initializer_list<std::string>& arguments);

    /// Creates a generator based on the given command-line arguments.
    ///
    /// @argc:   The number of command line arguments.  Since the program name
    ///          is always the first argument, this value must be 1 or
    ///          greater.
    /// @argv:   An array of argc command-line arguments.  The program name
    ///          MUST be the first argument.
    /// @input:  The input stream that the generator will obtain its data
    ///          string from if ordered to read from standard input
    ///          ("--input=-").
    /// @output: The output stream that the generator will write to by default
    ///          unless overridden by the arguments ("--output").
    Generator(int argc,
              const char* argv[],
              std::istream& input,
              std::ostream& output);

    /// Does the same thing as a call to Generator(argc, argv, cin, cout).
    Generator(int argc, const char* argv[]);

    /// Should we report our progress to stderr?
    bool verbose() const { return verbose_; }

    /// The X-dimensions of the lattice.
    int width() const { return width_; }

    /// The Y-dimensions of the lattice.
    int height() const { return height_; }

    /// The Z-dimensions of the lattice.
    int depth() const { return depth_; }

    ///////////////////////////////////////////////////////////////////////////
    // Functions which are only public because the unit tests need access to //
    // them.                                                                 //
    ///////////////////////////////////////////////////////////////////////////

    /// From which file should the input be read?  "-" means "standard input,"
    /// and "" (the default) means "we won't read from any input stream at
    /// all."  This latter was made the default so that insufficiently long
    /// data string arguments would be padded instead of causing the Generator
    /// to block for more input.
    ///
    /// This function is really only here for automated testing.
    const std::string& input() const { return input_file; }

    /// To which file should the results go?  "-" means "standard output."
    ///
    /// This function is really only here for automated testing.
    const std::string& output() const { return output_file; }

    /// The string data that was passed into us from the command line.
    ///
    /// This function is really only here for automated testing.
    const std::string& data() const { return data_; }

    /// An associative array that maps characters from the data string to the
    /// colors that the characters' corresponding cubes will represent.
    const std::map<wchar_t, RGBA> color_map() const { return color_map_; }

    /// The array of all the cubes in the final output object which were
    /// "activated" (as opposed to being missing or explicitly deactivated.)
    ///
    /// At most, there will be width() * height() * depth() cubes in the
    /// table, but it's quite possible for there to be fewer, or even none at
    /// all.  That's why I called it a "table" instead of a "lattice."
    const std::vector<Cube>& cube_table() const { return cube_table_; }

    /// The array of all the faces in the final output object which belong to
    /// cubes that were "activated" (as opposed to being missing or explicitly
    /// deactivated.)
    ///
    /// At most, there will be 3 * width() * height() * depth() + width() *
    /// height() + height() * depth() + width() * depth() such faces though,
    /// again, it is quite possible for there to be fewer, or even none at
    /// all.  That's why this is a "table" and not a "lattice."
    const std::vector<Face>& face_table() const { return face_table_; }

    /// The array of all the edges in the final output object which belong to
    /// cubes that were "activated" (as opposed to being missing or explicitly
    /// deactivated.)
    ///
    /// At most, there will be 3 * (width() + 1) * (height() + 1) * (depth() +
    /// 1) such edges.  Having fewer is quite possible, so we don't store
    /// these in a regular lattice like we do the vertices.
    const std::vector<Edge>& edge_table() const { return edge_table_; }

    /// The lattice of (W+1) * (H+1) * (D+1) vertices in the WxHxD cubic
    /// lattice.
    ///
    /// Note that not all of these will belong to a cube--that depends on the
    /// input data.
    const std::vector<Vertex>& vertex_lattice() const { return vertex_lattice_; }

    /// Converts the given coordinate into an offset in a linear array.
    ///
    /// I like representing 3D arrays using linear buffers; the grunt work to
    /// calculate the offsets may be harder, but it makes everything else
    /// easier down the road.
    static int offset(const Point& coordinate, int width, int height, int) {
      return (width * height) * coordinate.z + width * coordinate.y + coordinate.x;
    }

    /// Performs the reverse operation of offset(): converts a given offset
    /// into a coordinate.
    static Point coordinate(int offset, int width, int height, int) {
      Point result;
      result.z = static_cast<int>(offset / (width * height));
      result.y = static_cast<int>((offset - (width * height) * result.z) / width);
      result.x = (offset - (width * height) * result.z - width * result.y);
      return result;
    }

    /// Generates a compact vertex table from the vertex_lattice_, then
    /// generates over versions of all of the *other* tables which base their
    /// indexes on the new vertex table.
    ///
    /// We need to do this because the OFF file format is frugal, and only
    /// refers to vertices, edges, and faces which are actually in use.
    /// Unfortunately, while the vertex_lattice_ was convenient to use for
    /// generating the cubes to begin with, it may contain vertices which are
    /// never used and which we don't want to write to the OFF file.
    ///
    /// Simply skipping over such vertices is not an option, either; that
    /// would break our other data structures' indices.
    ///
    /// This is a helper function for emit_off_file().
    void create_compact_tables(std::vector<Vertex>& vertex_table,
                               std::vector<Edge>& edge_table,
                               std::vector<Face>& face_table,
                               std::vector<Cube>& cube_table) const;

  private:
    /// Does the main work of this program: gathering input, parsing it,
    /// generating an internal lattice, and writing out an OFF file.
    ///
    /// @arguments: The command-line arguments (the first of which MUST be the
    ///             program name.)  This is basically a smarter version of
    ///             argv[].
    /// @input:     The input stream that the generator will obtain its data
    ///             string from if ordered to read from standard input
    ///             ("--input=-").
    /// @output:    The output stream that we will write to by default unless
    ///             overridden by the arguments ("--output").
    void generate(const std::vector<std::string>& arguments,
                  std::istream& input,
                  std::ostream& output);

    /// Read tweakable parameters from argv and argc using getopt_long().
    ///
    /// @arguments: The command-line arguments (the first of which MUST be the
    ///             program name.)  This is basically a smarter version of
    ///             argv[].
    /// @input:     The input stream that the generator will obtain its data
    ///             string from if ordered to read from standard input
    ///             ("--input=-").
    /// @output:    The output stream that we will write to by default unless
    ///             overridden by the arguments ("--output").
    void parse_command_line(const std::vector<std::string>& arguments,
                            std::istream& input,
                            std::ostream& output);

    /// Prints a help message to the given output stream.
    ///
    /// This is a helper function for parse_command_line().
    void usage(const std::string& program_name, std::ostream& output_stream) const;

    /// Prints a version and copyright message to the given output stream.
    ///
    /// This is a helper function for parse_command_line().
    void display_version(std::ostream& out) const;

    /// Prints a message to stderr if and only if the verbose flag is set.
    void print(const std::string& message) const;

    /// Converts a string in the form "@=#rrggbbaa,#=#rrggbb,$=#rgba,%=#rgb"
    /// into an associative array whose keys are data string characters and
    /// whose values are colors (RGBA objects.)
    ///
    /// Recall that each "non-blank" character in the data string represents
    /// an individual cube.  This allows all sorts of characters to be treated
    /// equally like cubes, and affords us an impressive opportunity to
    /// identify "groups" of cubes and give each group the same color!
    ///
    /// This is a helper function for parse_command_line().
    void parse_color_list(const std::string& color_list);

    /// Combines all data string arguments and input stream input into one
    /// final, processed data string.
    ///
    /// This is a helper function for parse_command_line().
    std::string process_data_string(std::istream& input_stream,
                                    const std::string& combined_data_string_argument,
                                    const std::string& input_file_arugment);

    /// If the first few characters of the given input stream look like
    /// "/SOMA", this function will attempt to read the entire SOMA file from
    /// the input stream and store its contents in the result argument.
    ///
    /// Note that:
    ///
    /// * This method will change the width(), height(), and depth() so they
    ///   match that of the SOMA figure.
    /// * If the input_stream starts with "/SOMA" and proves to be an invalid
    ///   SOMA file, we consider that to be invalid input and throw an
    ///   exception.
    /// * If the input_stream does NOT start with "/SOMA", the input stream is
    ///   reverted its initial state so that it can be processed in full by
    ///   the caller.
    ///
    /// @param result  The data string that we got from the SOMA file.  If
    ///                the input was not a SOMA file, the string will be
    ///                unchanged, but no such guarantee is made if the input
    ///                stream was an *invalid* SOMA file.
    /// @returns       True if the input really was a SOMA file, and false
    ///                otherwise.
    bool read_soma_file(std::istream& input_stream, std::string& result);

    /// Parses the data string and attempts to convert it into a lattice
    /// (along with all other associated data structures.)
    ///
    /// This is a helper function for generate().
    void parse_data();

    /// Adds a cube to the system with the given lattice coordinate and
    /// color.
    ///
    /// By the time the function returns, it has also:
    /// - Created faces where needed
    /// - Created edges where needed
    /// - Updated vertex, edge, and face membership information
    ///
    /// This is a helper function for parse_data().
    void add_cube(Point p, const RGBA& color);

    /// Adds to the given cube the six faces corresponding to the given set of
    /// eight vertices.
    ///
    /// In the end, cube_table()[cube_index] is guaranteed to have its six
    /// faces() set.  However, this function may add fewer than six new faces
    /// to the system, depending on how many duplicate faces were detected.
    ///
    /// This is a helper function for add_cube().
    void add_faces(int cube_index,
                   int front_upper_left_vertex,
                   int front_upper_right_vertex,
                   int front_lower_left_vertex,
                   int front_lower_right_vertex,
                   int rear_upper_left_vertex,
                   int rear_upper_right_vertex,
                   int rear_lower_left_vertex,
                   int rear_lower_right_vertex);

    /// Goes through the entire face_table() and adds four Edge objects to
    /// each Face.
    ///
    /// If those Edge objects are unique, they are also added to the
    /// edge_table().  The edges inherit their respective faces' cubes() for
    /// the purpose of cross-referencing, and add themselves to those also.
    ///
    /// This is a helper function for parse_data().
    void add_edges();

    /// Goes through the given table of faces and mixes the colors of each
    /// face's parent cube(s) to create a final color that is then assigned to
    /// the face.
    ///
    /// We need the cube_table because faces only store the index of their
    /// parent cubes, not the parent cubes themselves.
    ///
    /// This is a helper function for parse_data().
    void assign_face_colors(const std::vector<Cube>& cube_table, std::vector<Face>& face_table);

    /// Goes through the given table of edges and mixes the colors of each
    /// edge's parent cube(s) to create a final color that is then assigned to
    /// the edge.
    ///
    /// This is a helper function for parse_data().
    void assign_edge_colors(const std::vector<Cube>& cube_table, std::vector<Edge>& edge_table);

    /// Goes through the given table of vertices and mixes the colors of each
    /// vertex's parent cube(s) to create a final color that is then assigned
    /// to the vertex.
    ///
    /// This is a helper function for parse_data().
    void assign_vertex_colors(const std::vector<Cube>& cube_table, std::vector<Vertex>& vertex_table);

    /// Searches for a face in the face_table() having the same vertex indices
    /// as the given face.  However, the precise order of vertices is ignored
    /// (i.e., {1, 2, 3, 4 } and { 4, 1, 3, 2 } are considered to be the same
    /// face.)
    ///
    /// This is a helper function for add_faces().
    ///
    /// @param proposed_face A face which is not in the system yet.
    /// @return The index of a face in the face_table() which matches the
    ///         given face's vertices, or -1 if no such face is found.
    int find_duplicate_face(const Face& proposed_face) const;

    /// Searches for an edge in the edge_table() having the same vertex
    /// indices as the given edge.  The order of vertices in each of the two
    /// edges is ignored for the purposes of this comparison.
    ///
    /// This is a helper function for add_edges().
    ///
    /// @param proposed_edge An edge which is not in the system yet.
    /// @return The index of an edge in the edge_table() which matches the
    ///         given edge's vertices, or -1 if no such edge is found.
    int find_duplicate_edge(const Edge& proposed_edge) const;

    /// Using the given (complete) data string, this function attempts to
    /// deduce the missing dimension if only two of the three given dimensions
    /// were "supplied" (that is, given non-default values.)
    ///
    /// Note that it is entirely possible for the product of the dimensions
    /// after this call to exceed the length of the data string!  For
    /// instance, if the data string's length was 14 and the width
    /// and height were each set to 4, the height would be calculated as 1,
    /// resulting in (4 * 4 * 1 - 14) = 2 characters of padding.
    ///
    /// This is a helper function for process_data_string().
    void deduce_dimensions(int& width, int& height, int& depth, const std::string& data);

    /// Writes an OFF file to the given output stream.
    ///
    /// The generated OFF file always employs the indexed_poly variant
    /// supported natively by Antiprism (and indeed, Antiprism compatibility
    /// was largely the point of writing this program in the first place.)
    /// See http://paulbourke.net/dataformats/off/ and
    /// http://paulbourke.net/dataformats/oogl/#OFF for more information on
    /// the file format.
    ///
    /// @param output_stream The stream to write the results to (generally
    ///                      stdout if the output_file parameter was "-" or an
    ///                      ofstream otherwise.)
    void emit_off_file(std::ostream& output_stream);

    int width_;
    int height_;
    int depth_;
    bool verbose_;
    std::string data_;
    std::string name_;
    std::string input_file;
    std::string output_file;
    bool remove_hidden_elements;
    std::vector<Vertex> vertex_lattice_;
    std::vector<Face> face_table_;
    std::vector<Edge> edge_table_;
    std::vector<Cube> cube_table_;
    std::map<wchar_t, RGBA> color_map_;

    /// The set of characters in the data string which *do not* represent cubes.
    std::string empty_characters;
};


#endif // #ifndef GENERATOR_H_INCLUDED__
