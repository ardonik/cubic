// =========================================================================
// Copyright (C) 2012 Ardonik <ardonik@ardonik.org>
//
// This file is part of ASC2OFF.
//
// ASC2OFF is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ASC2OFF is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ASC2OFF.  If not, see <http://www.gnu.org/licenses/>.
// =========================================================================

#ifndef EDGE_H_INCLUDED__
#define EDGE_H_INCLUDED__

#include <vector>
#include <array>
#include <iostream>
#include "rgba.h"

/// An edge is just an array of two points with some membership information
/// attached for cross-referencing.
class Edge
{
    public:
        Edge() : cubes_() { }

        /// This edge "owns" two vertices.  These are their offsets
        /// in the vertex lattice.
        const std::array<int, 2>& vertices() const { return vertices_; }
        std::array<int, 2>& vertices() { return vertices_; }

        /// This vertex can "belong" to up to four cubes.  These are their
        /// offsets in the cube table.
        const std::vector<int>& cubes() const { return cubes_; }
        std::vector<int>& cubes() { return cubes_; }

        /// Returns true if and only if both vertex indices belonging to the
        /// given edge also belong to us.
        bool has_same_vertices(const Edge& edge) const;

        /// Every edge has a color that is inherited from the parent faces.
        /// In the sort of lattice supported by this program, an edge can have
        /// up to four such parents.
        const RGBA& color() const { return color_; }
        RGBA& color() { return color_; }

        /// Prints an edge for debugging purposes.
        friend std::ostream& operator<< (std::ostream& out, const Edge& edge);

    private:
        std::vector<int> cubes_;
        std::array<int, 2> vertices_;
        RGBA color_;
};

#endif // #ifndef EDGE_H_INCLUDED__
