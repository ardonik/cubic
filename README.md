# CUBIC

**Cubic** is a command-line tool that takes _voxel_ structures as input and
converts them to various output formats.

A _voxel_, or volumetric pixel, is a unit cube in third-dimensional space.  It
may help you to think of Minecraft, but our voxels are far simpler: the only
voxel properties that we support are colors.

## Origin

I have had an interest in modular origami for over a decade now.  One of my
favorite units to explore is the **Sonobe module**, invented by Mitsonobu
Sonobe in the 1960s.  The completed unit lends itself to two different
interpretations:
- [Treating the unit as a square
face](https://www.flickr.com/photos/ardonik/3420209597/), connected
edge-to-edge with other square faces; and
- Treating the unit as a polyhedral edge, connected to other polyhedral edges
by means of their common vertices.

Both interpretations are _equally_ valid and lend themselves to the creation
of cubic 3D structures.  (The edge-based Sonobe modules can also make
triangular face, expanding the possible repertoire of models considerably.)

Learning this was a revelation for me: it meant that any connected cubic
structure I could think of could be realized with nothing more than pieces of
paper.  I quickly exhausted the simpler models, so to expand my repertoire, I
wrote this program to assist me in visualizing, and therefore folding, larger
structures.

## Antiprism

This program is expressly designed to work closely with
[Antiprism](https://antiprism.com), a suite of geometry manipulation tools
written by [Adrian Rossiter](http://www.antiprism.com/adrian/).  The tools
follow the Unix philosophy, with each tool specializing in manipulating
incoming geometry in the form of a [OFF
file](https://en.wikipedia.org/wiki/OFF_(file_format "Geomview Object File
Format")) and returning a slightly modified OFF file.  This pipeline can be
chained together in impressive ways.

The **Cubic** tool serves as an input to this pipeline only, allowing me to
take easily-written ASCII diagrams and turn them into geometry that I can
visualize.

## Command-line overview

```
Generates a set of connected cubes in the OFF file format.

Usage:

(1) bin/asc2off [-i SOMAFILE] [optional flags]
(2) bin/asc2off [-i TEXTFILE] [-w WIDTH] [-h HEIGHT] [-d DEPTH] [optional flags]
(3) bin/asc2off [-D DATASTRING] [-w WIDTH] [-h HEIGHT] [-d DEPTH] [optional flags]

SOMAFILE, if provided, must be in the SOLVER SOMA puzzle input file format.

Standard arguments:

  -w N,    --width=N      The width of the final object, in cubes.
  -h N,    --height=N     The height of the final object, in cubes.
  -d N,    --depth=N      The depth of the final object, in cubes.
  -D str,  --data=str     The data string in XZY order, one character per cube.
  -i file, --input=file   The data string input file.  Use '-' for stdin.

Optional arguments:

  -o file, --output=file  The OFF output file.  Defaults to '-' (stdout).
  -c list, --colors=list  A comma-separated list of character=color pairs.
  -O,      --optimize     Removes internal shared faces, edges, and vertices.
  -v,      --verbose      Prints out more information about what's going on.
  -V,      --version      Prints program version and license information.
  -H,      --help         Prints this help message.

If all three dimensions are provided, the data string will be truncated or
padded as necessary to meet the required number of cubes.  Either '-', '.',
 or ' ' can be used to represent an empty cube.  If both -D and -i are
provided, the input file's contents will come first in the data string.

Examples:

(1) A 3x3x1 ring made of eight cubes: -w 3 -h 3 -d 1 ####-####
(2) The same as above, but colored half red and half transparent blue: -c"A=#f00,B=#0000ff80" -w 3 -h 3 -d 1 AAAB-ABBB
(3) A 5x5 square pyramid: -w 5 -h 3 -d 5 ............#..................###..###..###......#########################

```

### Input formats

#### Text file input (`-i`, `--input`)

Text files passed in with `-i` that are *not* in the recognized SOLVE format
are interpreted as a WxHxD lattice of points.  The rest of the command line
supplies the dimensions, and indeed, it it is an _error_ to provide an input
in this format without explicitly specifying all three dimensions using the
`-w`, `-h`, and `-d` arguments.

All whitespace occurring within the data is ignored.  While it is conventional
to group the points in the text file into rows and to group the rows into
planes, the program does not require this.

##### XZY order

Let's define a 5x5x5 _cube of cubes_, with a differently-colored cube in the
middle.



    31113
    1   1
    1   1
    1   1
    31113

    1   1



    1   1

    1   1

      2

    1   1

    1   1



    1   1

    31113
    1   1
    1   1
    1   1
    31113


The structure is divided into five 5x5 planes of cubes.

- The X-axis, which corresponds to _width_, goes across the characters from
  left to right and resets after the end of every row.
- The Z-axis, which corresponds to _depth_, increases by 1 as we go down the
  rows of characters from top to bottom and resets after the end of every 5x5
  plane.
- The Y-axis, which corresponds to _height_ increases by 1 as skip from plane
  to plane.

The X-axis varies the most and the Y-axis varies the least, so we say that the
cubes are defined in **XZY order.**

Changing the dimensions can drastically alter the program's interpretation of the data.

| Width | Height | Depth | Appearance |
| ----- | ------ | ----- | ---------- |
| `-w5` | `-h5`  | `-d5` | ![A 5x5x5 rotating, hollow cubic voxel structure itself composed of cubes.](samples/basic-cube-1.300px.gif "Complete cube")|
| `-w5` | `-h3`  | `-d5` | ![The same 5x5x5 structure, but diminished by two planes due to decreased height.](samples/basic-cube-2.300px.gif "Partial cube (Y reduced)")|
| `-w3` | `-h5`  | `-d5` | ![The same 5x5x5 structure, but misinterpreted and mangled by dimishing the width .](samples/basic-cube-3.300px.gif "Mangled data (X reduced)")|


#### Datastrings (`-D`, `--data`)

This option allows you to pass in the same sort of non-SOLVER data that `-i`
would accept, but defined directly in the command-line arguments.  For
example:

    bin/asc2off -D "311131   11   11   1311131   1               1   11   1       2       1   11   1               1   1311131   11   11   131113" -w3 -h5 -d5  -c'1=#08f,2=#880,3=#f08' | ^Ctiview -xve -B black -R -90,0,0 &

produces the same 5x5x5 cubes of cubes illustrated in the previous section.

#### SOMA Solver files (`-i`, `--input`)

In 1999, [Thorleif Bundgård](https://www.fam-bundgaard.dk) and Courtney
McFarren jointly wrote a program called
[SOLVE](https://www.fam-bundgaard.dk/SOMA/SOLVER/SOLVER.HTM) that searches for
solutions to [SOMA Puzzle](https://www.fam-bundgaard.dk/SOMA/HISTORY.HTM)
figures.  SOLVE's input format, which has an informal specification
[here](https://www.fam-bundgaard.dk/SOMA/SOLVER/SOLVMANU.HTM#TEXT), turns out
to be an ideal way to represent voxels in plaintext.  There's nothing to
really improve upon, so the Cubic program will parse these files directly.

The input format is fairly straightforward, and since the dimensions of the
voxel field are unambiguous, **SOMA input files do not require you to pass in
dimension arguments.** The 5x5x5 cube of cubes might look like this as a SOLVE
file:

     /SOMAhi
     ; A hollow cube made of cubes to serve as an example for the Cubic project
     ; homepage.
     ;
     ; Suggested command line: bin/asc2off -i samples/hi-example.soma | antiview -B black -R -90,0,0
     /31113/1...1/1...1/1...1/31113
     /1...1/...../...../...../1...1
     /1...1/...../..2../...../1...1
     /1...1/...../...../...../1...1
     /31113/1...1/1...1/1...1/31113
